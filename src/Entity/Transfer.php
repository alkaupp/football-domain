<?php

declare(strict_types=1);

namespace Football\Domain\Entity;

use Football\Domain\Entity\Transfer\TransferOut;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

abstract class Transfer
{
    protected const TRANSFER_IN = "TRANSFER_IN";
    protected const TRANSFER_OUT = "TRANSFER_OUT";

    /** @var UuidInterface */
    private $id;

    /** @var Player */
    private $player;

    /** @var Team */
    private $team;

    /** @var \DateTimeImmutable */
    private $transfered;

    private $type;

    public function __construct(Player $player, Team $team, \DateTimeImmutable $transferDate, string $type)
    {
        $this->id = Uuid::uuid4();
        $this->player = $player;
        $this->team = $team;
        $this->transfered = $transferDate;
        $this->type = $type;
    }

    public function team(): Team
    {
        return $this->team;
    }

    public function compareTo(Transfer $transfer): int
    {
        $sortResults = $this->transfered <=> $transfer->transfered;
        if ($sortResults === 0) {
            return $transfer instanceof TransferOut ? -1 : 1;
        }
        return $sortResults;
    }
}
