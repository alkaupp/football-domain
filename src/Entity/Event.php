<?php

declare(strict_types=1);

namespace Football\Domain\Entity;

use Football\Domain\Objects\Event\EventInterface;
use Football\Domain\Objects\Match\MatchTime;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class Event implements EventInterface
{
    public const MATCH_STARTED = "match started";
    public const FIRST_HALF_ENDED = "first half ended";
    public const SECOND_HALF_STARTED = "second half started";
    public const MATCH_ENDED = "match ended";
    public const TYPE_GOAL = "goal";
    public const TYPE_OWN_GOAL = "own goal";
    public const TYPE_PENALTY_GOAL = "penalty goal";
    public const TYPE_SUBSTITUTION_IN = "substitution in";
    public const TYPE_SUBSTITUTION_OUT = "substitution out";
    public const TYPE_YELLOW_CARD = "yellow card";
    public const TYPE_SECOND_YELLOW_CARD = "second yellow card";
    public const TYPE_RED_CARD = "red card";

    private const GOAL_EVENTS = [
        Event::TYPE_GOAL,
        Event::TYPE_OWN_GOAL,
        Event::TYPE_PENALTY_GOAL
    ];

    private const SUBSTITUTION_EVENTS = [Event::TYPE_SUBSTITUTION_IN, Event::TYPE_SUBSTITUTION_OUT];

    private const BOOKING_EVENTS = [
        Event::TYPE_YELLOW_CARD,
        Event::TYPE_SECOND_YELLOW_CARD,
        Event::TYPE_RED_CARD
    ];

    private const SENDING_OFF_EVENTS = [
        Event::TYPE_SECOND_YELLOW_CARD,
        Event::TYPE_RED_CARD
    ];

    private const MATCH_STATUS_EVENTS = [
        self::MATCH_STARTED,
        self::MATCH_ENDED,
        self::FIRST_HALF_ENDED,
        self::SECOND_HALF_STARTED,
    ];

    /** @var UuidInterface */
    private $id;

    /** @var ?integer */
    private $tasoId;

    /** @var SoccerMatch */
    private $match;

    /** @var Player */
    protected $player;

    /** @var Team */
    protected $team;

    /** @var integer */
    private $period;

    /** @var string */
    private $time;

    /** @var string */
    private $name;

    public function __construct(SoccerMatch $match, MatchTime $matchTime, string $name, int $tasoId = null)
    {
        $this->id = Uuid::uuid4();
        $this->match = $match;
        $this->period = $matchTime->toPeriod();
        $this->time = $matchTime->toMinutesAndSeconds();
        $this->name = $name;
        $this->tasoId = $tasoId;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function time(): MatchTime
    {
        return MatchTime::fromTimeString($this->time, $this->period);
    }

    public function player(): Player
    {
        return $this->player;
    }

    public function compareTo(Event $event): int
    {
        return ($event->time <=> $this->time) * -1;
    }

    public function __toString(): string
    {
        return sprintf("%s %s %s: %s", $this->time(), $this->name, $this->team, $this->player->shirtName());
    }

    public function isPartOf(SoccerMatch $match): bool
    {
        return $this->match->equals($match);
    }

    public function isForHome(): bool
    {
        return $this->match->homeTeam()->equals($this->team);
    }

    public function isForAway(): bool
    {
        return $this->match->awayTeam()->equals($this->team);
    }

    public function isGoalEvent(): bool
    {
        return \in_array($this->name, self::GOAL_EVENTS, true);
    }

    public function isSubstitutionEvent(): bool
    {
        return \in_array($this->name, self::SUBSTITUTION_EVENTS, true);
    }

    public function isBookingEvent(): bool
    {
        return \in_array($this->name, self::BOOKING_EVENTS, true);
    }

    public function isSendingOffEvent(): bool
    {
        return \in_array($this->name, self::SENDING_OFF_EVENTS, true);
    }

    public function increasesHomeGoals(): bool
    {
        if ($this->name === self::TYPE_OWN_GOAL && $this->isForAway()) {
            return true;
        }
        if ($this->name === self::TYPE_OWN_GOAL && $this->isForHome()) {
            return false;
        }
        return $this->isGoalEvent() && $this->isForHome();
    }

    public function increasesAwayGoals(): bool
    {
        if ($this->name === self::TYPE_OWN_GOAL && $this->isForHome()) {
            return true;
        }
        if ($this->name === self::TYPE_OWN_GOAL && $this->isForAway()) {
            return false;
        }
        return $this->isGoalEvent() && $this->isForAway();
    }

    public function affectsMatchStatus(): bool
    {
        return \in_array($this->name, self::MATCH_STATUS_EVENTS, true);
    }
}
