<?php

declare(strict_types=1);

namespace Football\Domain\Entity;

use Football\Domain\Objects\Match\MatchTime;
use Football\Domain\Objects\NullPosition;
use Football\Domain\Objects\Position;
use JsonSerializable;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

abstract class Lineup implements JsonSerializable
{
    /** @var UuidInterface */
    private $id;

    /** @var ?integer */
    private $tasoId;

    /** @var SoccerMatch */
    private $match;

    /** @var Player */
    private $player;

    /** @var Team */
    private $team;

    /** @var integer */
    private $shirtNumber;

    /** @var bool */
    private $starting;

    /** @var integer */
    protected $playingTime;

    /** @var Position|string */
    private $position;

    /** @var ?integer */
    private $positionOrder;

    public function __construct(
        SoccerMatch $match,
        Player $player,
        Team $team,
        int $shirtNumber,
        bool $starting,
        int $playingTime,
        Position $position
    ) {
        $this->id = Uuid::uuid4();
        $this->match = $match;
        $this->player = $player;
        $this->team = $team;
        $this->shirtNumber = $shirtNumber;
        $this->starting = $starting;
        $this->playingTime = $playingTime;
        $this->position = $position->toShortDescription();
    }

    abstract public function substitute(MatchTime $matchTime): void;

    public function isForHome(): bool
    {
        return $this->match->homeTeam()->equals($this->team);
    }

    public function isForAway(): bool
    {
        return $this->match->awayTeam()->equals($this->team);
    }

    public function player(): Player
    {
        return $this->player;
    }

    public function shirtNumber(): int
    {
        return $this->shirtNumber;
    }

    public function jsonSerialize(): array
    {
        return [
            "player" => (string) $this->player,
            "shirtName" => $this->player->shirtName(),
            "shirtNumber" => $this->shirtNumber,
            "starting" => $this->starting,
            "position" => (string) $this->position,
            "playingTime" => $this->playingTime
        ];
    }

    public function compareTo(Lineup $lineup): int
    {
        if ($this->position()->toShortDescription() === "GK") {
            return -1;
        }

        if ($lineup->position()->toShortDescription() === "GK") {
            return 1;
        }

        return $this->shirtNumber <=> $lineup->shirtNumber;
    }

    public function isPartOf(SoccerMatch $match): bool
    {
        return $this->match->equals($match);
    }

    private function position(): Position
    {
        if (!$this->position) {
            return new NullPosition();
        }
        if (!$this->position instanceof Position) {
            return new Position($this->position);
        }
        return $this->position;
    }
}
