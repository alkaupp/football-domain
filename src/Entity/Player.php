<?php

declare(strict_types=1);

namespace Football\Domain\Entity;

use DateTime;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Football\Domain\Entity\Transfer\TransferIn;
use Football\Domain\Entity\Transfer\TransferOut;
use Football\Domain\Objects\Person;
use Football\Domain\Objects\Team\NullTeam;
use JsonSerializable;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class Player implements JsonSerializable
{
    private UuidInterface $id;
    private ?int $tasoId;
    private string $firstName;
    private string $lastName;
    private ?string $playerName;
    private DateTime | \DateTimeImmutable | null $birthday = null;
    private ?int $height;
    private ?int $weight;
    private ?string $nationality;
    /** @var Transfer[]|Collection */
    private Collection $transfers;

    public function __construct(Person $person, ?int $tasoId)
    {
        $this->id = Uuid::uuid4();
        $this->firstName = $person->getFirstName();
        $this->lastName = $person->getLastName();
        $this->playerName = $person->getNickName();
        $this->birthday = $person->getDateOfBirth();
        $this->height = $person->getHeight();
        $this->weight = $person->getWeight();
        $this->nationality = $person->getNationality();
        $this->tasoId = $tasoId;
        $this->transfers = new ArrayCollection();
    }

    public function __toString(): string
    {
        return sprintf('%s %s', $this->firstName, $this->lastName);
    }

    public function equals(Player $player): bool
    {
        return $player->id->equals($this->id);
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'playerName' => $this->playerName,
            'birthday' => $this->birthday?->format('Y-m-d') ?? '1970-01-01',
            'height' => $this->height,
            'weight' => $this->weight,
            'nationality' => $this->nationality
        ];
    }

    public function shirtName(): string
    {
        if ($this->playerName) {
            return $this->playerName;
        }
        return $this->lastName;
    }

    public function transferTo(Team $team): void
    {
        $lastTransfer = $this->transfers->last();
        if ($lastTransfer instanceof TransferIn) {
            $this->transfers->add(
                new TransferOut(
                    $this,
                    $lastTransfer->team(),
                    new DateTimeImmutable()
                )
            );
        }
        $this->transfers->add(new TransferIn($this, $team, new DateTimeImmutable()));
    }

    public function currentTeam(): Team
    {
        $transfers = $this->transfers->toArray();
        usort(
            $transfers,
            function (Transfer $first, Transfer $second) {
                return $first->compareTo($second);
            }
        );
        $transfers = new ArrayCollection($transfers);
        if ($transfers->last() instanceof TransferIn) {
            return $transfers->last()->team();
        }
        return new NullTeam();
    }

    public function tasoId(): ?int
    {
        return $this->tasoId;
    }
}
