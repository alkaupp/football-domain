<?php

declare(strict_types=1);

namespace Football\Domain\Entity\Transfer;

use Football\Domain\Entity\Player;
use Football\Domain\Entity\Team;
use Football\Domain\Entity\Transfer;

class TransferIn extends Transfer
{
    public function __construct(Player $player, Team $team, \DateTimeImmutable $transferDate)
    {
        parent::__construct($player, $team, $transferDate, self::TRANSFER_IN);
    }
}
