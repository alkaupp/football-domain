<?php

declare(strict_types=1);

namespace Football\Domain\Entity\Lineup;

use Football\Domain\Entity\Lineup;
use Football\Domain\Entity\SoccerMatch;
use Football\Domain\Entity\Player;
use Football\Domain\Entity\Team;
use Football\Domain\Objects\Match\MatchTime;
use Football\Domain\Objects\Position;

class StartingLineup extends Lineup
{
    public function __construct(SoccerMatch $match, Player $player, Team $team, int $shirtNumber, Position $position)
    {
        parent::__construct($match, $player, $team, $shirtNumber, true, 90, $position);
    }

    public function substitute(MatchTime $matchTime): void
    {
        $this->playingTime = $matchTime->toFullMinutes();
    }
}
