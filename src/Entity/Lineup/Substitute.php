<?php

declare(strict_types=1);

namespace Football\Domain\Entity\Lineup;

use Football\Domain\Entity\Lineup;
use Football\Domain\Entity\SoccerMatch;
use Football\Domain\Entity\Player;
use Football\Domain\Entity\Team;
use Football\Domain\Objects\Match\MatchTime;
use Football\Domain\Objects\NullPosition;

class Substitute extends Lineup
{
    public function __construct(SoccerMatch $match, Player $player, Team $team, int $shirtNumber)
    {
        parent::__construct($match, $player, $team, $shirtNumber, false, 0, new NullPosition());
    }

    public function substitute(MatchTime $matchTime): void
    {
        $this->playingTime = SoccerMatch::MATCH_LENGTH - $matchTime->toFullMinutes();
    }
}
