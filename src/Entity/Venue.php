<?php

declare(strict_types=1);

namespace Football\Domain\Entity;

use Football\Domain\Objects\Address;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class Venue
{
    /** @var UuidInterface */
    private $id;

    /** @var ?integer */
    private $tasoId;

    /** @var string */
    private $name;

    /** @var Address */
    private $address;

    public function __construct(string $name, Address $address)
    {
        $this->id = Uuid::uuid4();
        $this->name = $name;
        $this->address = $address;
    }

    public function __toString(): string
    {
        return $this->name;
    }
}
