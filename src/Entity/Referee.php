<?php

declare(strict_types=1);

namespace Football\Domain\Entity;

use Football\Domain\Objects\Nullable;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class Referee implements Nullable
{
    /** @var UuidInterface */
    private $id;

    /** @var ?integer */
    private $tasoId;

    /** @var string */
    private $name;

    public function __construct(string $name)
    {
        $this->id = Uuid::uuid4();
        $this->name = $name;
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function equals(Referee $referee): bool
    {
        return $this->id->equals($referee->id);
    }

    public function isNull(): bool
    {
        return false;
    }
}
