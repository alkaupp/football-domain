<?php

declare(strict_types=1);

namespace Football\Domain\Entity;

use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Football\Domain\Objects\Event\MatchEvents;
use Football\Domain\Objects\Lineups\Lineups;
use Football\Domain\Objects\Match\MatchResult;
use Football\Domain\Objects\Match\MatchStatistics;
use Football\Domain\Objects\Match\RefereeTeam;
use Football\Domain\Objects\Match\Score;
use JsonSerializable;
use LogicException;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class SoccerMatch implements JsonSerializable
{
    public const MATCH_LENGTH = 90;
    private const STATUS_PLANNED = 'Planned';
    private const STATUS_FIXTURE = 'Fixture';
    private const STATUS_LIVE = 'Live';
    private const STATUS_BREAK = 'Break';
    private const STATUS_PLAYED = 'Played';
    private const STATUS_CANCELED = 'Canceled';

    private UuidInterface $id;
    private ?int $tasoId;
    private Team $homeTeam;
    private Team $awayTeam;
    private ?Referee $referee;
    private ?Referee $firstAssistantReferee;
    private ?Referee $secondAssistantReferee;
    private ?Referee $fourthOfficial;
    private ?Venue $venue;
    private Competition $competition;
    private DateTimeInterface $startTime;
    private string $status;
    private ?int $attendance;
    private ?int $htsHome = 0;
    private ?int $htsAway = 0;
    private ?int $ftsHome = 0;
    private ?int $ftsAway = 0;

    /** @var Event[]|Collection */
    private Collection $events;

    /** @var Lineup[]|Collection */
    private Collection $lineups;

    public function __construct(
        Team $homeTeam,
        Team $awayTeam,
        DateTimeInterface $dateTime,
        Competition $competition,
        ?Venue $venue
    ) {
        $this->id = Uuid::uuid4();
        $this->htsHome = 0;
        $this->htsAway = 0;
        $this->ftsHome = 0;
        $this->ftsAway = 0;
        $this->homeTeam = $homeTeam;
        $this->awayTeam = $awayTeam;
        $this->referee = null;
        $this->firstAssistantReferee = null;
        $this->secondAssistantReferee = null;
        $this->fourthOfficial = null;
        $this->startTime = clone $dateTime;
        $this->competition = $competition;
        $this->venue = $venue;
        $this->status = self::STATUS_PLANNED;
        $this->attendance = null;
        $this->events = new ArrayCollection();
        $this->lineups = new ArrayCollection();
    }

    public function start(): void
    {
        $this->status = self::STATUS_LIVE;
    }

    public function end(int $attendance): void
    {
        if (!$this->isLive()) {
            throw new LogicException("Cannot end match that isn't live.");
        }
        $this->attendance = $attendance;
        $this->status = self::STATUS_PLAYED;
    }

    public function beginBreak(): void
    {
        if (!$this->isLive()) {
            throw new LogicException("Cannot break match that isn't live.");
        }
        $this->status = self::STATUS_BREAK;
    }

    public function endBreak(): void
    {
        $this->status = self::STATUS_LIVE;
    }

    public function cancel(): void
    {
        $this->status = self::STATUS_CANCELED;
    }

    public function assignRefereeTeam(RefereeTeam $refereeTeam): void
    {
        $this->referee = $refereeTeam->referee();
        $this->firstAssistantReferee = $refereeTeam->firstAssistant();
        $this->secondAssistantReferee = $refereeTeam->secondAssistant();
        $this->fourthOfficial = $refereeTeam->fourthOfficial()->isNull()
            ? null
            : $refereeTeam->fourthOfficial();
    }

    public function refereeTeamAssigned(): bool
    {
        return $this->referee && $this->firstAssistantReferee && $this->secondAssistantReferee;
    }

    public function isPlanned(): bool
    {
        return $this->status === self::STATUS_PLANNED;
    }

    public function isFixture(): bool
    {
        return $this->status === self::STATUS_FIXTURE;
    }

    public function isLive(): bool
    {
        return $this->status === self::STATUS_LIVE;
    }

    public function isPlayed(): bool
    {
        return $this->status === self::STATUS_PLAYED;
    }

    public function isCanceled(): bool
    {
        return $this->status === self::STATUS_CANCELED;
    }

    public function homeTeam(): Team
    {
        return $this->homeTeam;
    }

    public function awayTeam(): Team
    {
        return $this->awayTeam;
    }

    public function result(): MatchResult
    {
        if (!$this->isPlayed()) {
            throw new LogicException(sprintf('Match has not been played yet (%s).', $this->status));
        }
        return new MatchResult(
            $this->homeTeam,
            $this->awayTeam,
            new Score($this->ftsHome, $this->ftsAway),
            new Score($this->htsHome, $this->htsAway)
        );
    }

    public function addLineup(Lineup $lineup): void
    {
        if (!$lineup->isPartOf($this)) {
            throw new LogicException("Lineup doesn't belong to match");
        }
        $this->lineups[] = $lineup;
    }

    public function addEvent(Event $event): void
    {
        if (!$event->isPartOf($this)) {
            throw new LogicException("Event doesn't belong to match");
        }
        if ($this->triggersSubstitution($event)) {
            $lineup = $this->lineups()->for($event->player());
            $lineup->substitute($event->time());
        }
        if ($event->isGoalEvent()) {
            $this->addGoalEvent($event);
        }
        if ($event->affectsMatchStatus()) {
            $this->addStatusChangeEvent($event);
        }
        $this->events[] = $event;
    }

    public function lineups(): Lineups
    {
        $lineups = new Lineups($this);
        foreach ($this->lineups as $lineup) {
            $lineups->addLineup($lineup);
        }
        return $lineups;
    }

    public function statistics(): MatchStatistics
    {
        $stats = new MatchStatistics();
        foreach ($this->events as $event) {
            $stats->addEvent($event);
        }
        return $stats;
    }

    public function matchEvents(): MatchEvents
    {
        $matchEvents = new MatchEvents($this);
        foreach ($this->events as $event) {
            $matchEvents->addEvent($event);
        }
        return $matchEvents;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'startTime' => $this->startTime->format('Y-m-d H:i'),
            'venue' => $this->venue ? (string) $this->venue : null,
            'homeTeam' => $this->homeTeam,
            'awayTeam' => $this->awayTeam,
            'halfTimeScore' => (string) (new Score((int) $this->htsHome, (int) $this->htsAway)),
            'fullTimeScore' => (string) (new Score((int) $this->ftsHome, (int) $this->ftsAway)),
            'referee' => $this->referee ? (string) $this->referee : null,
            'firstAssistantReferee' => $this->firstAssistantReferee ? (string) $this->firstAssistantReferee : null,
            'secondAssistantReferee' => $this->firstAssistantReferee ? (string) $this->secondAssistantReferee : null,
            'fourthOfficial' => $this->fourthOfficial ? (string) $this->fourthOfficial : null,
            'status' => $this->status,
            'attendance' => $this->attendance,
            'matchEvents' => $this->matchEvents()
        ];
    }

    public function equals(SoccerMatch $match): bool
    {
        return $this->id->equals($match->id);
    }

    private function triggersSubstitution(Event $event): bool
    {
        return $event->isSubstitutionEvent() || $event->isSendingOffEvent();
    }

    private function addGoalEvent(Event $event): void
    {
        if ($event->increasesHomeGoals()) {
            $this->ftsHome++;
            if ($event->time()->toPeriod() === 1) {
                $this->htsHome++;
            }
        } elseif ($event->increasesAwayGoals()) {
            $this->ftsAway++;
            if ($event->time()->toPeriod() === 1) {
                $this->htsAway++;
            }
        }
    }

    private function addStatusChangeEvent(Event $event): void
    {
        if ($event->name() === Event::MATCH_STARTED) {
            $this->status = self::STATUS_LIVE;
        }
        if ($event->name() === Event::FIRST_HALF_ENDED) {
            $this->status = self::STATUS_BREAK;
        }
        if ($event->name() === Event::SECOND_HALF_STARTED) {
            $this->status = self::STATUS_LIVE;
        }
        if ($event->name() === Event::MATCH_ENDED) {
            $this->status = self::STATUS_PLAYED;
        }
    }

    public function addTasoId(int $tasoId): void
    {
        $this->tasoId = $tasoId;
    }

    public function tasoId(): ?int
    {
        return $this->tasoId;
    }

    public function startTime(): DateTimeImmutable
    {
        return DateTimeImmutable::createFromMutable($this->startTime);
    }
}
