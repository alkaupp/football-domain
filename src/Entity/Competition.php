<?php

declare(strict_types=1);

namespace Football\Domain\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Football\Domain\Objects\Competition\CompetitionException;
use Football\Domain\Objects\Competition\Standing;
use Football\Domain\Objects\Competition\Standings;
use JsonSerializable;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class Competition implements JsonSerializable
{
    private const STATUS_PLANNED = "Planned";
    private const STATUS_IN_PROGRESS = "In progress";
    private const STATUS_FINISHED = "Finished";
    private const STATUS_CANCELED = "Canceled";
    
    /** @var UuidInterface */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $status;

    /** @var Team[]|Collection */
    private $teams;

    /** @var SoccerMatch[]|Collection */
    private $matches;

    /** @var PointReduction[]|Collection */
    private $pointReductions;

    /** @var Standings */
    private $standings;

    public function __construct(string $name)
    {
        $this->id = Uuid::uuid4();
        $this->name = $name;
        $this->status = self::STATUS_PLANNED;
        $this->teams = new ArrayCollection();
        $this->matches = new ArrayCollection();
        $this->pointReductions = new ArrayCollection();
    }

    public function isPlanned(): bool
    {
        return $this->status === self::STATUS_PLANNED;
    }

    public function isRunning(): bool
    {
        return $this->status === self::STATUS_IN_PROGRESS;
    }

    public function isFinished(): bool
    {
        return $this->status === self::STATUS_FINISHED;
    }

    public function isCanceled(): bool
    {
        return $this->status === self::STATUS_CANCELED;
    }

    public function getMatches(callable $callback): Collection
    {
        $matches = new ArrayCollection();
        foreach ($this->matches as $match) {
            if ($callback($match)) {
                $matches->add($match);
            }
        }
        return $matches;
    }

    public function start(): void
    {
        if ($this->isRunning()) {
            throw new CompetitionException("Competition is already running!");
        }

        if ($this->isFinished()) {
            throw new CompetitionException("Cannot start a competition that is finished!");
        }
        $this->status = self::STATUS_IN_PROGRESS;
    }

    public function finish(): void
    {
        if ($this->isPlanned()) {
            throw new CompetitionException("Cannot finish competition that is not started!");
        }

        if ($this->isFinished()) {
            throw new CompetitionException("Competition already finished");
        }
        foreach ($this->matches as $match) {
            if (!$match->isPlayed()) {
                throw new CompetitionException("Cannot finish competition until all matches are played!");
            }
        }
        $this->status = self::STATUS_FINISHED;
    }

    public function cancel(): void
    {
        if ($this->isFinished()) {
            throw new CompetitionException("Cannot cancel a finished competition!");
        }

        if ($this->isCanceled()) {
            throw new CompetitionException("Competition already canceled!");
        }
        $this->status = self::STATUS_CANCELED;
    }


    public function addMatch(Team $homeTeam, Team $awayTeam, DateTime $dateTime, ?Venue $venue): void
    {
        $this->throwExceptionForRelation("match");
        $this->matches[] = new SoccerMatch($homeTeam, $awayTeam, $dateTime, $this, $venue);
    }

    public function addTeam(Team $team): void
    {
        $this->throwExceptionForRelation("team");
        $this->teams[] = $team;
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function jsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "name" => $this->name
        ];
    }

    public function standings(): Standings
    {
        if ($this->standings !== null) {
            return $this->standings;
        }
        $this->standings = new Standings();
        $this->standings->addTeams($this->teams);
        $this->standings->addMatches($this->matches);
        $this->standings->addPointReductions($this->pointReductions);
        return $this->standings;
    }

    private function throwExceptionForRelation(string $relation): void
    {
        if ($this->isRunning()) {
            throw new CompetitionException("Cannot add {$relation} to a running competition!");
        }

        if ($this->isFinished()) {
            throw new CompetitionException("Cannot add {$relation} to a finished competition!");
        }

        if ($this->isCanceled()) {
            throw new CompetitionException("Cannot add {$relation} to a canceled competition!");
        }
    }
}
