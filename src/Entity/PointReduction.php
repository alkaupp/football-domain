<?php

declare(strict_types=1);

namespace Football\Domain\Entity;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class PointReduction
{
    /** @var UuidInterface */
    private $id;

    /** @var Competition */
    private $competition;

    /** @var Team */
    private $team;

    /** @var integer */
    private $reduction;

    /** @var string */
    private $reason;

    public function __construct(Competition $competition, Team $team, int $reduction, string $reason)
    {
        $this->id = Uuid::uuid4();
        $this->competition = $competition;
        $this->team = $team;
        $this->reduction = $reduction;
        $this->reason = $reason;
    }

    public function team(): Team
    {
        return $this->team;
    }

    public function reduction(): int
    {
        return $this->reduction;
    }
}
