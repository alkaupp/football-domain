<?php

declare(strict_types=1);

namespace Football\Domain\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Football\Domain\Objects\SocialMedia;
use JsonSerializable;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class Team implements JsonSerializable
{
    /** @var UuidInterface */
    private $id;

    /** @var ?integer */
    private $tasoId;

    /** @var string */
    private $name;

    /** @var ?integer */
    private $founded;

    /** @var ?string */
    private $homepage;

    /** @var ?string */
    private $facebook;

    /** @var ?string */
    private $twitter;

    /** @var ?string */
    private $youtube;

    /** @var ?string */
    private $instagram;

    /** @var Player[]|Collection */
    private $players;

    /** @var Competition[]|Collection */
    private $competitions;

    /** @var PointReduction[]|Collection */
    private $pointReductions;

    public function __construct(string $name, SocialMedia $socialMedia, ?int $founded)
    {
        $this->id = Uuid::uuid4();
        $this->name = $name;
        $this->homepage = $socialMedia->getHomepage();
        $this->facebook = $socialMedia->getFacebook();
        $this->twitter = $socialMedia->getTwitter();
        $this->youtube = $socialMedia->getYoutube();
        $this->instagram = $socialMedia->getInstagram();
        $this->founded = $founded;
        $this->players = new ArrayCollection();
        $this->competitions = new ArrayCollection();
        $this->pointReductions = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function equals(Team $team): bool
    {
        return $this->id->equals($team->id);
    }

    public function players(): Collection
    {
        return $this->players;
    }

    public function jsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "founded" => $this->founded,
            "socialMedia" => [
                "homepage" => $this->homepage,
                "facebook" => $this->facebook,
                "twitter" => $this->twitter,
                "youtube" => $this->youtube,
                "instagram" => $this->instagram
            ],
        ];
    }

    public function addTasoId(int $tasoId): void
    {
        $this->tasoId = $tasoId;
    }

    public function tasoId(): ?int
    {
        return $this->tasoId;
    }
}
