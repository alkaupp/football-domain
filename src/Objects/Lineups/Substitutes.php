<?php

declare(strict_types=1);

namespace Football\Domain\Objects\Lineups;

use Football\Domain\Entity\Lineup\Substitute;

class Substitutes extends LineupCollection
{
    private const MAX_SUBSTITUTES = 7;
    private const MIN_SORTABLE_SUBSTITUTES = 4;

    protected function minimumSortableLineups(): int
    {
        return self::MIN_SORTABLE_SUBSTITUTES;
    }

    public function add(Substitute $substitute): void
    {
        if ($this->count() === self::MAX_SUBSTITUTES) {
            throw new LineupsException("Substitutes already contains " . self::MAX_SUBSTITUTES . " players");
        }

        parent::addLineup($substitute);
    }
}
