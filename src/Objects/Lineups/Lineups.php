<?php

declare(strict_types=1);

namespace Football\Domain\Objects\Lineups;

use Countable;
use DomainException;
use Football\Domain\Entity\Lineup;
use Football\Domain\Entity\Lineup\StartingLineup;
use Football\Domain\Entity\Lineup\Substitute;
use Football\Domain\Entity\SoccerMatch;
use Football\Domain\Entity\Player;
use Football\Domain\Entity\Team;
use JsonSerializable;

class Lineups implements JsonSerializable, Countable
{
    /** @var SoccerMatch */
    private $match;

    /** @var StartingLineups */
    private $homeStarting;

    /** @var StartingLineups */
    private $awayStarting;

    /** @var Substitutes */
    private $homeSubstitutes;

    /** @var Substitutes */
    private $awaySubstitutes;

    public function __construct(SoccerMatch $match)
    {
        $this->match = $match;
        $this->homeStarting = new StartingLineups();
        $this->awayStarting = new StartingLineups();
        $this->homeSubstitutes = new Substitutes();
        $this->awaySubstitutes = new Substitutes();
    }

    public function homeStarters(): StartingLineups
    {
        return $this->homeStarting;
    }

    public function homeSubstitutes(): Substitutes
    {
        return $this->homeSubstitutes;
    }

    public function awayStarters(): StartingLineups
    {
        return $this->awayStarting;
    }

    public function awaySubstitutes(): Substitutes
    {
        return $this->awaySubstitutes;
    }

    public function jsonSerialize(): array
    {
        return [
            "home" => [
                "starting" => $this->homeStarting,
                "substitutes" => $this->homeSubstitutes
            ],
            "away" => [
                "starting" => $this->awayStarting,
                "substitutes" => $this->awaySubstitutes
            ]
        ];
    }

    public function addLineup(Lineup $lineup): void
    {
        if (!$lineup->isPartOf($this->match)) {
            throw new DomainException("Lineup is not part of match.");
        }

        if ($lineup instanceof StartingLineup) {
            $this->addStartingPlayer($lineup);
        } elseif ($lineup instanceof Substitute) {
            $this->addSubstitute($lineup);
        }
    }

    private function addStartingPlayer(StartingLineup $starter): void
    {
        if ($starter->isForHome()) {
            $this->homeStarting->add($starter);
        } elseif ($starter->isForAway()) {
            $this->awayStarting->add($starter);
        }
    }

    private function addSubstitute(Substitute $substitute): void
    {
        if ($substitute->isForHome()) {
            $this->homeSubstitutes->add($substitute);
        } elseif ($substitute->isForAway()) {
            $this->awaySubstitutes->add($substitute);
        }
    }

    public function count()
    {
        return count($this->homeStarting)
            + count($this->homeSubstitutes)
            + count($this->awayStarting)
            + count($this->awaySubstitutes);
    }

    public function for(Player $player): Lineup
    {
        $lineupCollections = [
            $this->homeStarting,
            $this->homeSubstitutes,
            $this->awayStarting,
            $this->awaySubstitutes
        ];

        /** @var LineupCollection $collection */
        foreach ($lineupCollections as $collection) {
            try {
                return $collection->for($player);
            } catch (LineupsException $e) {
                continue;
            }
        }
        throw new LineupsException("No lineup for player {$player}");
    }
}
