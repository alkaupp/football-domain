<?php

declare(strict_types=1);

namespace Football\Domain\Objects\Lineups;

use Football\Domain\Entity\Lineup\StartingLineup;

class StartingLineups extends LineupCollection
{
    private const MAX_STARTERS = 11;

    protected function minimumSortableLineups(): int
    {
        return self::MAX_STARTERS;
    }

    public function add(StartingLineup $lineup): void
    {
        if ($this->count() === self::MAX_STARTERS) {
            throw new LineupsException("StartingLineups already contains " . self::MAX_STARTERS . " players");
        }
        parent::addLineup($lineup);
    }
}
