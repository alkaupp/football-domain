<?php

declare(strict_types=1);

namespace Football\Domain\Objects\Lineups;

use ArrayIterator;
use Countable;
use Football\Domain\Entity\Lineup;
use Football\Domain\Entity\Player;
use Iterator;
use IteratorAggregate;
use JsonSerializable;

abstract class LineupCollection implements IteratorAggregate, Countable, JsonSerializable
{
    /** @var Lineup[] */
    private $lineups = [];

    /**
     * How many lineups is required in collection for it to be sorted?
     * This is needed to avoid excessive sorting of collections
     * @return int
     */
    abstract protected function minimumSortableLineups(): int;

    protected function addLineup(Lineup $lineup): void
    {
        if ($this->playerIsInLineups($lineup->player())) {
            throw new LineupsException("Player {$lineup->player()} is already in lineups!");
        }
        $this->lineups[] = $lineup;

        if ($this->count() >= $this->minimumSortableLineups()) {
            $this->sort();
        }
    }

    public function for(Player $player): Lineup
    {
        /** @var Lineup $lineup */
        foreach ($this as $lineup) {
            if ($lineup->player()->equals($player)) {
                return $lineup;
            }
        }
        throw new LineupsException("No lineup for player {$player}");
    }

    public function getIterator(): Iterator
    {
        return new ArrayIterator($this->lineups);
    }

    public function count(): int
    {
        return count($this->lineups);
    }

    public function jsonSerialize(): array
    {
        return $this->lineups;
    }

    private function playerIsInLineups(Player $player): bool
    {
        foreach ($this->lineups as $lineup) {
            if ($lineup->player()->equals($player)) {
                return true;
            }
        }
        return false;
    }

    private function sort(): void
    {
        usort(
            $this->lineups,
            function (Lineup $lineup1, Lineup $lineup2) {
                return $lineup1->compareTo($lineup2);
            }
        );
    }
}
