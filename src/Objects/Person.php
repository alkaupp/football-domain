<?php

declare(strict_types=1);

namespace Football\Domain\Objects;

use DateTime;
use DateTimeImmutable;

class Person
{
    /** @var string */
    private $firstName;

    /** @var string */
    private $lastName;

    /** @var ?string */
    private $nickName;

    /** @var DateTimeImmutable */
    private $dateOfBirth;

    /** @var ?integer */
    private $height;

    /** @var ?integer */
    private $weight;

    /** @var ?string */
    private $nationality;

    public function __construct(
        string $firstName,
        string $lastName,
        DateTime $dateOfBirth,
        ?string $nickName,
        ?int $height,
        ?int $weight,
        ?string $nationality
    ) {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->dateOfBirth = new DateTimeImmutable($dateOfBirth->format("Y-m-d"));
        $this->nickName = $nickName;
        $this->height = $height;
        $this->weight = $weight;
        $this->nationality = $nationality;
    }

    public static function fromArray(array $person): Person
    {
        return new Person(
            $person["first_name"] ?? "",
            $person["last_name"] ?? "",
            isset($person["birthday"]) ? new DateTime($person["birthday"]) : new DateTime(),
            $person["nick_name"] ?? null,
            isset($person["height"]) ? (int) $person["height"] : null,
            isset($person["weight"]) ? (int) $person["weight"] : null,
            $person["nationality"] ?? null
        );
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getNickName(): ?string
    {
        return $this->nickName;
    }

    public function getDateOfBirth(): DateTimeImmutable
    {
        return $this->dateOfBirth;
    }

    public function getHeight(): ?int
    {
        return $this->height;
    }

    public function getWeight(): ?int
    {
        return $this->weight;
    }

    public function getNationality(): ?string
    {
        return $this->nationality;
    }

    public function __toString(): string
    {
        return "{$this->lastName}, {$this->firstName}";
    }
}
