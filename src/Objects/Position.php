<?php

declare(strict_types=1);

namespace Football\Domain\Objects;

use InvalidArgumentException;

class Position implements Nullable
{
    private const POSITIONS = [
        "GK" => "Goalkeeper",
        "DEF" => "Defender",
        "LB" => "Defender, left",
        "LCB" => "Defender, center left",
        "CB" => "Defender, center",
        "RCB" => "Defender, center right",
        "RB" => "Defender, right",
        "WBL" => "Wing back, left",
        "WBR" => "Wing back, right",
        "MF" => "Midfield",
        "DML" => "Midfield, defensive left",
        "DMC" => "Midfield, defensive center",
        "DMR" => "Midfield, defensive right",
        "LM" => "Midfield, left",
        "LCM" => "Midfield, center left",
        "CM" => "Midfield, center",
        "RCM" => "Midfield, center right",
        "RM" => "Midfield, right",
        "AML" => "Midfield, attacking left",
        "AMC" => "Midfield, attacking center",
        "AMR" => "Midfield, attacking right",
        "LW" => "Winger, left",
        "RW" => "Winger, right",
        "FW" => "Forward",
        "LF" => "Forward, left",
        "CF" => "Forward, center",
        "RF" => "Forward, right",
        "ST" => "Striker",
        "NULL" => "Null position"
    ];

    private $position;

    public function __construct(string $position)
    {
        $this->position = strtoupper($position);
        $this->checkSanity($this->position);
    }

    public function toShortDescription(): string
    {
        return $this->position;
    }

    private function checkSanity(string $position): void
    {
        if (!array_key_exists($position, self::POSITIONS)) {
            throw new InvalidArgumentException("Invalid position {$position}");
        }
    }

    public function __toString(): string
    {
        return self::POSITIONS[$this->position];
    }

    public function isNull(): bool
    {
        return false;
    }
}
