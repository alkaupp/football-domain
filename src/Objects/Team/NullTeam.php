<?php

declare(strict_types=1);

namespace Football\Domain\Objects\Team;

use Football\Domain\Entity\Team;
use Football\Domain\Objects\SocialMedia;

class NullTeam extends Team
{
    public function __construct()
    {
        parent::__construct("", new SocialMedia(), null);
    }
}
