<?php

declare(strict_types=1);

namespace Football\Domain\Objects\Referee;

use Football\Domain\Entity\Referee;

class NullReferee extends Referee
{
    public function __construct()
    {
        parent::__construct("");
    }

    public function isNull(): bool
    {
        return true;
    }

    public function equals(Referee $referee): bool
    {
        return false;
    }
}
