<?php

declare(strict_types=1);

namespace Football\Domain\Objects;

class Address
{
    /** @var string */
    private $streetAddress;

    /** @var string */
    private $postalCode;

    /** @var string */
    private $city;

    /** @var string */
    private $country;

    public function __construct(string $streetAddress, string $postalCode, string $city, string $country)
    {
        $this->streetAddress = $streetAddress;
        $this->postalCode = $postalCode;
        $this->city = $city;
        $this->country = $country;
    }

    public function getStreetAddress(): string
    {
        return $this->streetAddress;
    }

    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public function __toString(): string
    {
        return "{$this->streetAddress}, {$this->postalCode}, {$this->city}, {$this->country}";
    }
}
