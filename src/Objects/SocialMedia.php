<?php

declare(strict_types=1);

namespace Football\Domain\Objects;

/**
 * @method ?string getHomepage()
 * @method ?string getFacebook()
 * @method ?string getYoutube()
 * @method ?string getTwitter()
 * @method ?string getInstagram()
 */
class SocialMedia
{
    private $media = [];

    public function add(string $media, string $url): void
    {
        $this->media[$media] = $url;
    }

    public function __call(string $method, array $arguments): ?string
    {
        $media = str_replace("get", "", strtolower($method));
        if (isset($this->media[$media])) {
            return $this->media[$media];
        }
        return null;
    }
}
