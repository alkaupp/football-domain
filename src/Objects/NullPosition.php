<?php

declare(strict_types=1);

namespace Football\Domain\Objects;

class NullPosition extends Position
{
    public function __construct()
    {
        parent::__construct("NULL");
    }

    public function toShortDescription(): string
    {
        return "";
    }

    public function __toString(): string
    {
        return "";
    }

    public function isNull(): bool
    {
        return true;
    }
}
