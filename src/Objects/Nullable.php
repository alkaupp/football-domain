<?php

declare(strict_types=1);

namespace Football\Domain\Objects;

interface Nullable
{
    public function isNull(): bool;
}
