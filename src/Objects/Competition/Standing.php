<?php

declare(strict_types=1);

namespace Football\Domain\Objects\Competition;

use Football\Domain\Entity\Team;
use JsonSerializable;

class Standing implements JsonSerializable
{
    private const VICTORY_POINTS = 3;
    private const DRAW_POINTS = 1;

    /** @var Team */
    private $team;

    /** @var integer */
    private $ranking;

    /** @var integer */
    private $matchesPlayed;

    /** @var integer */
    private $points;

    /** @var integer */
    private $goalsScored;

    /** @var integer */
    private $goalsConceded;

    /** @var integer */
    private $goalDifference;

    public function __construct(Team $team)
    {
        $this->team = $team;
        $this->ranking = 0;
        $this->matchesPlayed = 0;
        $this->points = 0;
        $this->goalsScored = 0;
        $this->goalsConceded = 0;
        $this->goalDifference = 0;
    }

    public function addResult(int $teamGoals, int $opponentGoals): void
    {
        $this->matchesPlayed++;
        $this->goalsScored += $teamGoals;
        $this->goalsConceded += $opponentGoals;
        $this->goalDifference += ($teamGoals - $opponentGoals);
        if ($teamGoals > $opponentGoals) {
            $this->points += self::VICTORY_POINTS;
        } elseif ($teamGoals === $opponentGoals) {
            $this->points += self::DRAW_POINTS;
        }
    }

    public function decreasePoints(int $points): void
    {
        $this->points -= $points;
    }

    public function rank(int $ranking): void
    {
        $this->ranking = $ranking;
    }

    public function compareTo(Standing $standing): int
    {
        if ($this->points() > $standing->points()) {
            return -1;
        } elseif ($this->points() < $standing->points()) {
            return 1;
        }

        if ($this->goalDifference() > $standing->goalDifference()) {
            return -1;
        } elseif ($this->goalDifference() < $standing->goalDifference()) {
            return 1;
        }

        if ($this->goalsScored() > $standing->goalsScored()) {
            return -1;
        } elseif ($this->goalsScored() < $standing->goalsScored()) {
            return 1;
        }
        return (string) $this->team() <=> (string) $standing->team();
    }

    public function team(): Team
    {
        return $this->team;
    }

    public function ranking(): int
    {
        return $this->ranking;
    }

    public function matchesPlayed(): int
    {
        return $this->matchesPlayed;
    }

    public function points(): int
    {
        return $this->points;
    }

    public function goalsScored(): int
    {
        return $this->goalsScored;
    }

    public function goalsConceded(): int
    {
        return $this->goalsConceded;
    }

    public function goalDifference(): int
    {
        return $this->goalDifference;
    }

    public function jsonSerialize()
    {
        return [
            "team" => (string) $this->team,
            "ranking" => $this->ranking,
            "matchesPlayed" => $this->matchesPlayed,
            "points" => $this->points,
            "goalDifference" => $this->goalDifference,
            "goalsScored" => $this->goalsScored,
            "goalsConceded" => $this->goalsConceded
        ];
    }
}
