<?php

declare(strict_types=1);

namespace Football\Domain\Objects\Competition;

use ArrayIterator;
use Doctrine\Common\Collections\Collection;
use Exception;
use Football\Domain\Entity\SoccerMatch;
use Football\Domain\Entity\PointReduction;
use Football\Domain\Entity\Team;
use Iterator;
use IteratorAggregate;
use JsonSerializable;

class Standings implements IteratorAggregate, JsonSerializable
{
    private $standings = [];

    public function addTeams(Collection $teams): void
    {
        foreach ($teams as $team) {
            $this->standings[] = new Standing($team);
        }
        $this->order();
    }

    public function addMatches(Collection $matches): void
    {
        /** @var SoccerMatch $match */
        foreach ($matches as $match) {
            if (!$match->isPlayed()) {
                continue;
            }
            $this->addMatch($match);
        }
        $this->order();
    }

    public function addPointReductions(Collection $pointReductions): void
    {
        /** @var PointReduction $reduction */
        foreach ($pointReductions as $reduction) {
            $standing = $this->findStandingByTeam($reduction->team());
            $standing->decreasePoints($reduction->reduction());
        }
        $this->order();
    }

    public function getIterator(): Iterator
    {
        return new ArrayIterator($this->standings);
    }

    public function jsonSerialize(): array
    {
        return [
            "standings" => $this->standings
        ];
    }

    private function addMatch(SoccerMatch $match): void
    {
        $homeStanding = $this->findStandingByTeam($match->homeTeam());
        $awayStanding = $this->findStandingByTeam($match->awayTeam());
        $result = $match->result();
        $homeStanding->addResult($result->fullTimeScore()->forHome(), $result->fullTimeScore()->forAway());
        $awayStanding->addResult($result->fullTimeScore()->forAway(), $result->fullTimeScore()->forHome());
    }

    /**
     * @param   Team $team
     * @return  Standing
     * @throws  Exception
     */
    private function findStandingByTeam(Team $team): Standing
    {
        /** @var Standing $standing */
        foreach ($this->standings as $standing) {
            if ($standing->team()->equals($team)) {
                return $standing;
            }
        }
        throw new StandingsException(sprintf("Could not find Standing for team %s", (string) $team));
    }

    private function order(): void
    {
        usort(
            $this->standings,
            function (Standing $st1, Standing $st2) {
                return $st1->compareTo($st2);
            }
        );
        $counter = 1;
        /** @var Standing $standing */
        foreach ($this->standings as $standing) {
            $standing->rank($counter);
            $counter++;
        }
    }
}
