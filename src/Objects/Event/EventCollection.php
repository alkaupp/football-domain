<?php

declare(strict_types=1);

namespace Football\Domain\Objects\Event;

class EventCollection extends AbstractEventCollection
{
    public function add(EventInterface $event): void
    {
        $this->addToCollection($event);
    }
}
