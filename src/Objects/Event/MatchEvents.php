<?php

declare(strict_types=1);

namespace Football\Domain\Objects\Event;

use Football\Domain\Entity\Event;
use Football\Domain\Entity\SoccerMatch;
use Iterator;
use IteratorAggregate;
use JsonSerializable;

class MatchEvents implements JsonSerializable, IteratorAggregate
{
    private Goals $goals;
    private Substitutions $substitutions;
    private Bookings $bookings;

    public function __construct(SoccerMatch $match)
    {
        $this->goals = new Goals($match);
        $this->bookings = new Bookings();
        $this->substitutions = new Substitutions($match);
    }

    public function addEvent(Event $event): void
    {
        if ($event->isGoalEvent()) {
            $this->goals->addEvent($event);
        } elseif ($event->isBookingEvent()) {
            $this->bookings->addEvent($event);
        } elseif ($event->isSubstitutionEvent()) {
            $this->substitutions->addEvent($event);
        }
    }

    public function goals(): Goals
    {
        return $this->goals;
    }

    public function substitutions(): Substitutions
    {
        return $this->substitutions;
    }

    public function bookings(): Bookings
    {
        return $this->bookings;
    }

    public function jsonSerialize(): array
    {
        return [
            'goals' => $this->goals,
            'substitutions' => $this->substitutions,
            'bookings' => $this->bookings
        ];
    }

    /**
     * @return Iterator<EventInterface>
     * @throws \Exception
     */
    public function getIterator(): Iterator
    {
        $eventCollection = new EventCollection();
        /** @var AbstractEventCollection $collection */
        foreach ([$this->goals, $this->substitutions, $this->bookings] as $collection) {
            foreach ($collection as $event) {
                $eventCollection->add($event);
            }
        }
        return $eventCollection->getIterator();
    }
}
