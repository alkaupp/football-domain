<?php

declare(strict_types=1);

namespace Football\Domain\Objects\Event;

class EventNotFoundException extends \Exception
{

}
