<?php

declare(strict_types=1);

namespace Football\Domain\Objects\Event;

class OwnGoal extends Goal
{
    private const NAME = "own goal";

    public function name(): string
    {
        return self::NAME;
    }

    public function __toString(): string
    {
        return sprintf(
            "%s %s (OG) %s: %s %d-%d",
            $this->time(),
            parent::name(),
            $this->team(),
            $this->player()->shirtName(),
            $this->score()->forHome(),
            $this->score()->forAway()
        );
    }
}
