<?php

declare(strict_types=1);

namespace Football\Domain\Objects\Event;

use Exception;
use Football\Domain\Entity\Event;
use Football\Domain\Entity\SoccerMatch;
use Football\Domain\Entity\Player;

class Bookings extends AbstractEventCollection
{
    public function addEvent(Event $event): void
    {
        parent::addToCollection($event);
    }

    public function isBooked(Player $player): bool
    {
        try {
            $this->bookingFor($player);
            return true;
        } catch (EventNotFoundException $e) {
            return false;
        }
    }

    public function isSentOff(Player $player): bool
    {
        try {
            $this->sendingOffFor($player);
            return true;
        } catch (EventNotFoundException $e) {
            return false;
        }
    }

    /**
     * @param Player $player
     * @return Event
     * @throws EventNotFoundException
     */
    public function bookingFor(Player $player): Event
    {
        return $this->getFor(
            $player,
            function (Event $event, Player $player) {
                return $event->isBookingEvent() && $event->player()->equals($player);
            },
            "No booking found for {$player}"
        );
    }

    /**
     * @param Player $player
     * @return Event
     * @throws EventNotFoundException
     */
    public function sendingOffFor(Player $player): Event
    {
        return $this->getFor(
            $player,
            function (Event $event, Player $player) {
                return $event->isSendingOffEvent() && $event->player()->equals($player);
            },
            "{$player} hasn't been sent off"
        );
    }

    private function getFor(Player $player, callable $criteria, string $exceptionMessage): Event
    {
        /** @var Event $event */
        foreach ($this as $event) {
            if ($criteria($event, $player)) {
                return $event;
            }
        }
        throw new EventNotFoundException($exceptionMessage);
    }
}
