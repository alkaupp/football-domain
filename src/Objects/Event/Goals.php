<?php

declare(strict_types=1);

namespace Football\Domain\Objects\Event;

use Football\Domain\Entity\Event;
use Football\Domain\Entity\SoccerMatch;
use Football\Domain\Entity\Player;
use Football\Domain\Objects\Match\Score;
use LogicException;

class Goals extends AbstractEventCollection
{
    private $homeGoals = 0;
    private $awayGoals = 0;

    /** @var SoccerMatch */
    private $match;

    public function __construct(SoccerMatch $match)
    {
        $this->match = $match;
    }

    public function addEvent(Event $event): void
    {
        $team = $event->isForHome()
            ? $this->match->homeTeam()
            : $this->match->awayTeam();
        parent::addToCollection(Goal::createFromEvent($event, $this->createScore($event), $team));
    }

    public function addGoal(Goal $goal): void
    {
        parent::addToCollection($goal);
    }

    public function hasScored(Player $player): bool
    {
        try {
            $this->for($player);
            return true;
        } catch (EventNotFoundException $e) {
            return false;
        }
    }

    public function for(Player $player): Goals
    {
        $goals = new self($this->match);
        /** @var Goal $goal */
        foreach ($this as $goal) {
            if ($goal->player()->equals($player)) {
                $goals->addGoal($goal);
            }
        }
        if ($goals->isEmpty()) {
            throw new EventNotFoundException("{$player} has not scored in match");
        }
        return $goals;
    }

    public function first(): Goal
    {
        if ($this->isEmpty()) {
            throw new LogicException("Collection is empty!");
        }
        $this->getIterator()->rewind();
        return $this->getIterator()->current();
    }

    public function __toString(): string
    {
        $output = "";
        /** @var Goal $goal */
        foreach ($this as $goal) {
            $output .= (string) $goal . "\n";
        }
        return $output;
    }

    private function createScore(Event $event): Score
    {
        if ($event->isForHome()) {
            if ($event->name() === Event::TYPE_OWN_GOAL) {
                $this->awayGoals++;
            } else {
                $this->homeGoals++;
            }
        } elseif ($event->isForAway()) {
            if ($event->name() === Event::TYPE_OWN_GOAL) {
                $this->homeGoals++;
            } else {
                $this->awayGoals++;
            }
        }
        return new Score($this->homeGoals, $this->awayGoals);
    }
}
