<?php

declare(strict_types=1);

namespace Football\Domain\Objects\Event;

use Football\Domain\Entity\Player;
use Football\Domain\Entity\Team;
use Football\Domain\Objects\Match\MatchTime;

class Substitution implements EventInterface
{
    private const NAME = "substitution";

    /** @var Player */
    private $inPlayer;

    /** @var Player */
    private $outPlayer;

    /** @var MatchTime */
    private $time;

    /** @var Team */
    private $team;

    public function __construct(Player $inPlayer, Player $outPlayer, MatchTime $time, Team $team)
    {
        $this->inPlayer = $inPlayer;
        $this->outPlayer = $outPlayer;
        $this->time = $time;
        $this->team = $team;
    }

    public function inPlayer(): Player
    {
        return $this->inPlayer;
    }

    public function outPlayer(): Player
    {
        return $this->outPlayer;
    }

    public function time(): MatchTime
    {
        return $this->time;
    }

    public function __toString(): string
    {
        return sprintf(
            "%s %s %s: IN %s, OUT %s",
            $this->time,
            $this->name(),
            $this->team,
            $this->inPlayer->shirtName(),
            $this->outPlayer->shirtName()
        );
    }

    public function name(): string
    {
        return self::NAME;
    }
}
