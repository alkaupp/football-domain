<?php

declare(strict_types=1);

namespace Football\Domain\Objects\Event;

use ArrayIterator;
use Countable;
use IteratorAggregate;
use JsonSerializable;

abstract class AbstractEventCollection implements IteratorAggregate, Countable, JsonSerializable
{
    /** @var EventInterface[] */
    private $events = [];

    protected function addToCollection(EventInterface $event): void
    {
        $this->events[] = $event;
        $this->sort();
    }

    final public function isEmpty(): bool
    {
        return count($this->events) === 0;
    }

    final public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->events);
    }

    final public function count(): int
    {
        return count($this->events);
    }

    public function jsonSerialize(): array
    {
        return array_map(
            function ($event) {
                return (string) $event;
            },
            $this->events
        );
    }

    private function sort(): void
    {
        usort(
            $this->events,
            function (EventInterface $event1, EventInterface $event2) {
                if (!method_exists($event1, "time") && !method_exists($event2, "time")) {
                    return 0;
                }
                return $event1->time()->compareTo($event2->time());
            }
        );
    }
}
