<?php

declare(strict_types=1);

namespace Football\Domain\Objects\Event;

use Football\Domain\Entity\Event;
use Football\Domain\Entity\Player;
use Football\Domain\Entity\Team;
use Football\Domain\Objects\Match\MatchTime;
use Football\Domain\Objects\Match\Score;
use InvalidArgumentException;

class Goal implements EventInterface
{
    private const NAME = "goal";

    /** @var Player */
    private $player;

    /** @var MatchTime */
    private $time;

    /** @var Score */
    private $score;

    /** @var Team */
    private $team;

    public function __construct(Player $player, MatchTime $time, Score $result, Team $team)
    {
        $this->player = $player;
        $this->time = $time;
        $this->score = $result;
        $this->team = $team;
    }

    public static function createFromEvent(Event $event, Score $score, Team $team): Goal
    {
        $type = $event->name();
        if ($type === Event::TYPE_GOAL) {
            return new Goal($event->player(), $event->time(), $score, $team);
        }

        if ($type === Event::TYPE_PENALTY_GOAL) {
            return new PenaltyGoal($event->player(), $event->time(), $score, $team);
        }

        if ($type === Event::TYPE_OWN_GOAL) {
            return new OwnGoal($event->player(), $event->time(), $score, $team);
        }
        throw new InvalidArgumentException("Invalid goal type: {$type}");
    }

    public function player(): Player
    {
        return $this->player;
    }

    public function time(): MatchTime
    {
        return $this->time;
    }

    public function score(): Score
    {
        return $this->score;
    }

    public function __toString(): string
    {
        return sprintf(
            "%s %s %s: %s %d-%d",
            $this->time,
            $this->name(),
            $this->team,
            $this->player->shirtName(),
            $this->score->forHome(),
            $this->score->forAway()
        );
    }

    public function name(): string
    {
        return self::NAME;
    }

    protected function team(): Team
    {
        return $this->team;
    }
}
