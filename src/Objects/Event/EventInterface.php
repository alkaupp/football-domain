<?php

declare(strict_types=1);

namespace Football\Domain\Objects\Event;

use Football\Domain\Objects\Match\MatchTime;

interface EventInterface
{
    public function name(): string;
    public function time(): MatchTime;
    public function __toString(): string;
}
