<?php

declare(strict_types=1);

namespace Football\Domain\Objects\Event;

use Football\Domain\Entity\Event;
use Football\Domain\Entity\SoccerMatch;
use Football\Domain\Entity\Player;

class Substitutions extends AbstractEventCollection
{
    private $substitutionEvents = [];
    /** @var SoccerMatch */
    private $match;

    public function __construct(SoccerMatch $match)
    {
        $this->match = $match;
    }

    public function addEvent(Event $event): void
    {
        $this->substitutionEvents[] = $event;
        $this->pairEvents();
    }

    public function addSubstitution(Substitution $substitution): void
    {
        parent::addToCollection($substitution);
    }

    public function isSubstituted(Player $player): bool
    {
        try {
            $this->for($player);
            return true;
        } catch (SubstitutionNotFoundException $e) {
            return false;
        }
    }

    public function for(Player $player): Substitution
    {
        /** @var Substitution $substitution */
        foreach ($this as $substitution) {
            if ($substitution->inPlayer()->equals($player)) {
                return $substitution;
            }

            if ($substitution->outPlayer()->equals($player)) {
                return $substitution;
            }
        }
        throw new SubstitutionNotFoundException("Substitution not found for player {$player}");
    }

    private function pairEvents(): void
    {
        /**
         * @var integer $key
         * @var Event $event
         */
        foreach ($this->substitutionEvents as $key => $event) {
            if ($this->isSubstituted($event->player())) {
                continue;
            }
            try {
                $matchingEvent = $this->findMatchingSubstitutionEvent($event);
                $team = $event->isForHome()
                    ? $this->match->homeTeam()
                    : $this->match->awayTeam();
                $substitution = new Substitution(
                    $this->getInPlayer($event, $matchingEvent),
                    $this->getOutPlayer($event, $matchingEvent),
                    $event->time(),
                    $team
                );
                unset(
                    $this->substitutionEvents[$key],
                    $this->substitutionEvents[array_search($matchingEvent, $this->substitutionEvents, true)]
                );
                $this->addSubstitution($substitution);
            } catch (MatchEventsException $e) {
                continue;
            }
        }
    }

    /**
     * @param Event $event
     * @return Event
     * @throws MatchEventsException
     */
    private function findMatchingSubstitutionEvent(Event $event): Event
    {
        /** @var Event $substitutionEvent */
        foreach ($this->substitutionEvents as $substitutionEvent) {
            if ($this->substitutionEventMatches($event, $substitutionEvent)) {
                return $substitutionEvent;
            }
        }
        throw new MatchEventsException("Cannot find matching substitution for event.");
    }

    private function substitutionEventMatches(Event $event1, Event $event2): bool
    {
        if ($event1->name() === $event2->name()) {
            return false;
        }

        if (!$event1->time()->equals($event2->time())) {
            return false;
        }

        if (
            ($event1->isForHome() && $event2->isForAway())
            || ($event1->isForAway() && $event2->isForHome())
        ) {
            return false;
        }

        return true;
    }

    private function getInPlayer(Event $substitution1, Event $substitution2): Player
    {
        if ($substitution1->name() === Event::TYPE_SUBSTITUTION_IN) {
            return $substitution1->player();
        }

        if ($substitution2->name() === Event::TYPE_SUBSTITUTION_IN) {
            return $substitution2->player();
        }
        throw new MatchEventsException(sprintf("Neither event is type '%s'", Event::TYPE_SUBSTITUTION_IN));
    }

    private function getOutPlayer(Event $substitution1, Event $substitution2): Player
    {
        if ($substitution1->name() === Event::TYPE_SUBSTITUTION_OUT) {
            return $substitution1->player();
        }

        if ($substitution2->name() === Event::TYPE_SUBSTITUTION_OUT) {
            return $substitution2->player();
        }
        throw new MatchEventsException(sprintf("Neither event is type '%s'", Event::TYPE_SUBSTITUTION_OUT));
    }
}
