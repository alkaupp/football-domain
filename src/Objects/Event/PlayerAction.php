<?php

declare(strict_types=1);

namespace Football\Domain\Objects\Event;

use Football\Domain\Entity\Event;
use Football\Domain\Entity\SoccerMatch;
use Football\Domain\Entity\Player;
use Football\Domain\Entity\Team;
use Football\Domain\Objects\Match\MatchTime;

class PlayerAction extends Event
{
    public function __construct(
        SoccerMatch $match,
        Player $player,
        Team $team,
        MatchTime $matchTime,
        string $name,
        int $tasoId = null
    ) {
        parent::__construct($match, $matchTime, $name, $tasoId);
        $this->player = $player;
        $this->team = $team;
    }
}
