<?php

declare(strict_types=1);

namespace Football\Domain\Objects\Match;

class MatchStatistic
{
    /** @var string */
    private $description;

    /** @var integer */
    private $homeCount;

    /** @var integer */
    private $awayCount;

    public function __construct(string $description)
    {
        $this->description = $description;
        $this->homeCount = 0;
        $this->awayCount = 0;
    }

    public function addHomeEvent(): void
    {
        $this->homeCount++;
    }

    public function addAwayEvent(): void
    {
        $this->awayCount++;
    }

    public function forHome(): int
    {
        return $this->homeCount;
    }

    public function forAway(): int
    {
        return $this->awayCount;
    }

    public function description(): string
    {
        return $this->description;
    }
}
