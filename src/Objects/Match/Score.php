<?php

declare(strict_types=1);

namespace Football\Domain\Objects\Match;

final class Score
{
    private $homeScore;
    private $awayScore;

    public function __construct(int $homeScore, int $awayScore)
    {
        $this->homeScore = $homeScore;
        $this->awayScore = $awayScore;
    }

    public function forHome(): int
    {
        return $this->homeScore;
    }

    public function forAway(): int
    {
        return $this->awayScore;
    }

    public function __toString(): string
    {
        return sprintf("%d - %d", $this->homeScore, $this->awayScore);
    }
}
