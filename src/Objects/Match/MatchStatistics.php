<?php

declare(strict_types=1);

namespace Football\Domain\Objects\Match;

use ArrayIterator;
use Football\Domain\Entity\Event;
use IteratorAggregate;

class MatchStatistics implements IteratorAggregate
{
    /** @var MatchStatistic[] */
    private $statistics;

    public function __construct()
    {
        $this->statistics = [];
    }

    public function addEvent(Event $event): void
    {
        try {
            $stat = $this->getStatistic($event->name());
        } catch (MatchStatisticNotFoundException $e) {
            $stat = new MatchStatistic($event->name());
            $this->statistics[] = $stat;
        }
        if ($event->isForHome()) {
            $stat->addHomeEvent();
        } elseif ($event->isForAway()) {
            $stat->addAwayEvent();
        }
    }

    public function addStatistic(string $description): void
    {
        if (!$this->hasStatistic($description)) {
            $this->statistics[] = new MatchStatistic($description);
        }
    }

    public function hasStatistic(string $description): bool
    {
        try {
            $this->getStatistic($description);
            return true;
        } catch (MatchStatisticNotFoundException $e) {
            return false;
        }
    }

    public function getStatistic(string $description): MatchStatistic
    {
        foreach ($this->statistics as $statistic) {
            if ($statistic->description() === $description) {
                return $statistic;
            }
        }
        throw new MatchStatisticNotFoundException("No statistic described {$description}.");
    }

    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->statistics);
    }
}
