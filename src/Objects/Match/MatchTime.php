<?php

declare(strict_types=1);

namespace Football\Domain\Objects\Match;

class MatchTime
{
    private $minutes;
    private $seconds;
    private $period;

    private const FIRST_HALF_MAX_MINUTES = 45;
    private const SECOND_HALF_MAX_MINUTES = 90;

    public function __construct(int $minutes, int $seconds, int $period)
    {
        $this->minutes = $minutes;
        $this->seconds = $seconds;
        $this->period = $period;
    }

    public static function fromTimeString(string $time, int $period): MatchTime
    {
        list($minutes, $seconds) = explode(":", $time);
        if (preg_match("/[+]/", $minutes)) {
            list($regularTime, $additionalTime) = explode("+", $minutes);
            $minutes = (int) $regularTime + (int) $additionalTime;
        }
        return new MatchTime((int) $minutes, (int) $seconds, $period);
    }

    public function __toString(): string
    {
        if ($this->period === 1) {
            return $this->createString(self::FIRST_HALF_MAX_MINUTES);
        }
        return $this->createString(self::SECOND_HALF_MAX_MINUTES);
    }

    public function toMinutesAndSeconds(): string
    {
        if ($this->period === 1 && $this->minutes >= self::FIRST_HALF_MAX_MINUTES) {
            $minutes = self::FIRST_HALF_MAX_MINUTES;
            $additionalTime = $this->minutes - self::FIRST_HALF_MAX_MINUTES;
            return sprintf("%d+%02d:%d", $minutes, $additionalTime, $this->seconds);
        }
        if ($this->period === 2 && $this->minutes >= self::SECOND_HALF_MAX_MINUTES) {
            $minutes = self::SECOND_HALF_MAX_MINUTES;
            $additionalTime = $this->minutes - self::SECOND_HALF_MAX_MINUTES;
            return sprintf("%d+%02d:%d", $minutes, $additionalTime, $this->seconds);
        }

        return sprintf("%02d:%02d", $this->minutes, $this->seconds);
    }

    public function toFullMinutes(): int
    {
        return $this->minutes;
    }

    public function getSeconds(): int
    {
        return $this->seconds;
    }

    public function toSeconds(): int
    {
        return ($this->minutes * 60) + $this->seconds;
    }

    public function toPeriod(): int
    {
        return $this->period;
    }

    public function equals(MatchTime $matchTime): bool
    {
        return $this->toSeconds() === $matchTime->toSeconds();
    }

    public function compareTo(MatchTime $matchTime): int
    {
        return $this->toSeconds() <=> $matchTime->toSeconds();
    }

    private function createString(int $limit): string
    {
        if ($this->minutes < $limit) {
            return sprintf("%d'", $this->minutes + 1);
        }
        $additionalTime = ($this->minutes - $limit) + 1;
        return sprintf("%d+%d'", $limit, $additionalTime);
    }
}
