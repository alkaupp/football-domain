<?php

declare(strict_types=1);

namespace Football\Domain\Objects\Match;

use Football\Domain\Entity\Team;

class MatchResult
{
    private $homeTeam;
    private $awayTeam;
    private $fullTimeScore;
    private $halfTimeScore;

    public function __construct(
        Team $homeTeam,
        Team $awayTeam,
        Score $fullTimeScore,
        Score $halftimeScore
    ) {
        $this->homeTeam = $homeTeam;
        $this->awayTeam = $awayTeam;
        $this->fullTimeScore = $fullTimeScore;
        $this->halfTimeScore = $halftimeScore;
    }

    public function halfTimeScore(): Score
    {
        return $this->halfTimeScore;
    }

    public function fullTimeScore(): Score
    {
        return $this->fullTimeScore;
    }

    public function __toString(): string
    {
        return "{$this->homeTeam} {$this->fullTimeScore} {$this->awayTeam}";
    }
}
