<?php

declare(strict_types=1);

namespace Football\Domain\Objects\Match;

use Football\Domain\Entity\Referee;
use Football\Domain\Objects\Referee\NullReferee;

class RefereeTeam
{
    /** @var Referee */
    private $referee;

    /** @var Referee */
    private $firstAssistant;

    /** @var Referee */
    private $secondAssistant;

    /** @var ?Referee */
    private $fourthOfficial;

    public function __construct(
        Referee $referee,
        Referee $firstAssistant,
        Referee $secondAssistant,
        ?Referee $fourthOfficial
    ) {
        $this->referee = $referee;
        $this->firstAssistant = $firstAssistant;
        $this->secondAssistant = $secondAssistant;
        $this->fourthOfficial = $fourthOfficial;
    }

    public function referee(): Referee
    {
        return $this->referee;
    }

    public function firstAssistant(): Referee
    {
        return $this->firstAssistant;
    }

    public function secondAssistant(): Referee
    {
        return $this->secondAssistant;
    }

    public function fourthOfficial(): Referee
    {
        return $this->fourthOfficial ?? new NullReferee();
    }
}
