<?php

declare(strict_types=1);

namespace Football\Domain\Factory;

use Football\Domain\Entity\Lineup;
use Football\Domain\Entity\Lineup\StartingLineup;
use Football\Domain\Entity\Lineup\Substitute;
use Football\Domain\Entity\SoccerMatch;
use Football\Domain\Entity\Player;
use Football\Domain\Entity\Team;
use Football\Domain\Objects\Position;

class LineupFactory
{
    public function createStarter(
        SoccerMatch $match,
        Player $player,
        Team $team,
        Position $position,
        int $shirtNumber
    ): Lineup {
        return new StartingLineup($match, $player, $team, $shirtNumber, $position);
    }

    public function createSubstitute(SoccerMatch $match, Player $player, Team $team, int $shirtNumber): Lineup
    {
        return new Substitute($match, $player, $team, $shirtNumber);
    }
}
