<?php

declare(strict_types=1);

namespace Football\Domain\Repository;

use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\Collection;
use Football\Domain\Entity\Competition;
use Football\Domain\Entity\SoccerMatch;
use Football\Domain\Entity\Team;
use Ramsey\Uuid\UuidInterface;

interface SoccerMatchRepository
{
    public function getMatch(UuidInterface $uuid): SoccerMatch;

    public function getByTasoId(int $tasoId): SoccerMatch;

    public function findMatchByDateAndTeam(DateTime $date, Team $team): ?SoccerMatch;


    /**
     * @param   Competition             $competition
     * @param   Team|null               $team
     * @param   DateTimeInterface|null  $date
     * @param   DateTimeInterface|null  $startDate
     * @param   DateTimeInterface|null  $endDate
     * @return  SoccerMatch[]|Collection
     */
    public function getMatches(
        Competition $competition,
        ?Team $team,
        ?DateTimeInterface $date,
        ?DateTimeInterface $startDate,
        ?DateTimeInterface $endDate
    ): Collection;

    public function save(SoccerMatch $match): void;
}
