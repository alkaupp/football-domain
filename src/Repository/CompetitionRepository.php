<?php

declare(strict_types=1);

namespace Football\Domain\Repository;

use Doctrine\Common\Collections\Collection;
use Football\Domain\Entity\Competition;
use Football\Domain\Entity\SoccerMatch;
use Football\Domain\Entity\Team;
use Ramsey\Uuid\UuidInterface;

interface CompetitionRepository
{
    public function getCompetition(UuidInterface $uuid): Competition;

    public function getByName(string $name): Competition;

    public function getCompetitions(): Collection;

    /**
     * @param string $competitionName
     * @return  Team[]|Collection
     */
    public function getCompetitionTeams(string $competitionName): Collection;

    /**
     * @param   UuidInterface   $uuid
     * @param   array           $parameters
     * @return  SoccerMatch[]|Collection
     */
    public function getCompetitionMatches(UuidInterface $uuid, array $parameters): Collection;

    public function save(Competition $competition): void;
}
