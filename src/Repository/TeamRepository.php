<?php

declare(strict_types=1);

namespace Football\Domain\Repository;

use Doctrine\Common\Collections\Collection;
use Football\Domain\Entity\Competition;
use Football\Domain\Entity\Team;
use Ramsey\Uuid\UuidInterface;

interface TeamRepository
{
    public function getTeam(UuidInterface $uuid): Team;

    public function findByName(string $name): Team;

    /**
     * @return Team[]|Collection
     */
    public function getTeams(): Collection;

    /**
     * @param   Competition $competition
     * @return  Team[]|Collection
     */
    public function getByCompetition(Competition $competition): Collection;

    public function save(Team $team): void;
}
