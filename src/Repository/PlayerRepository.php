<?php

declare(strict_types=1);

namespace Football\Domain\Repository;

use Doctrine\Common\Collections\Collection;
use Football\Domain\Entity\Player;
use Ramsey\Uuid\UuidInterface;

interface PlayerRepository
{
    public function getAll(): Collection;

    public function getPlayer(UuidInterface $uuid): Player;

    public function save(Player $player): void;
}
