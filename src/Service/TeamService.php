<?php

declare(strict_types=1);

namespace Football\Domain\Service;

use Doctrine\Common\Collections\Collection;
use Football\Domain\Entity\Player;
use Football\Domain\Entity\Team;
use Football\Domain\Repository\CompetitionRepository;
use Football\Domain\Repository\TeamRepository;
use Ramsey\Uuid\UuidInterface;

class TeamService
{
    /** @var TeamRepository */
    private $repository;

    /** @var CompetitionRepository */
    private $competitionRepository;

    public function __construct(TeamRepository $repository, CompetitionRepository $competitionRepository)
    {
        $this->repository = $repository;
        $this->competitionRepository = $competitionRepository;
    }

    public function getTeam(UuidInterface $uuid): Team
    {
        return $this->repository->getTeam($uuid);
    }

    public function getTeams(): Collection
    {
        return $this->repository->getTeams();
    }

    public function getByCompetitionName(string $competitionName): Collection
    {
        $competition = $this->competitionRepository->getByName($competitionName);
        $teams = $this->repository->getByCompetition($competition);
        return $teams;
    }

    /**
     * @param UuidInterface $uuid
     * @return Player[]|Collection
     */
    public function getTeamPlayers(UuidInterface $uuid): Collection
    {
        $team = $this->getTeam($uuid);

        return $team->players();
    }
}
