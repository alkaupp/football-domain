<?php

declare(strict_types=1);

namespace Football\Domain\Service;

use Doctrine\Common\Collections\Collection;
use Football\Domain\Entity\Competition;
use Football\Domain\Objects\Competition\Standings;
use Football\Domain\Repository\CompetitionRepository;
use Ramsey\Uuid\UuidInterface;

class CompetitionService
{
    /** @var CompetitionRepository */
    private $competitionRepository;

    public function __construct(CompetitionRepository $repository)
    {
        $this->competitionRepository = $repository;
    }

    public function getCompetitions(): Collection
    {
        return $this->competitionRepository->getCompetitions();
    }

    public function getCompetition(UuidInterface $uuid): Competition
    {
        return $this->competitionRepository->getCompetition($uuid);
    }

    public function getCompetitionTeams(string $competitionName): Collection
    {
        return $this->competitionRepository->getCompetitionTeams($competitionName);
    }

    public function getCompetitionMatches(UuidInterface $uuid, array $parameters): Collection
    {
        return $this->competitionRepository->getCompetitionMatches($uuid, $parameters);
    }

    public function getCompetitionStandings(UuidInterface $uuid): Standings
    {
        $competition = $this->getCompetition($uuid);
        return $competition->standings();
    }
}
