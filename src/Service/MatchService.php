<?php

declare(strict_types=1);

namespace Football\Domain\Service;

use Football\Domain\Entity\SoccerMatch;
use Football\Domain\Objects\Lineups\Lineups;
use Football\Domain\Objects\Lineups\LineupsException;
use Football\Domain\Repository\SoccerMatchRepository;
use Ramsey\Uuid\UuidInterface;

class MatchService
{
    /** @var SoccerMatchRepository */
    private $repository;

    public function __construct(SoccerMatchRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getMatch(UuidInterface $uuid): SoccerMatch
    {
        return $this->repository->getMatch($uuid);
    }

    /**
     * @param   UuidInterface $uuid
     * @return  Lineups
     */
    public function getMatchLineups(UuidInterface $uuid): Lineups
    {
        $match = $this->repository->getMatch($uuid);
        return $match->lineups();
    }
}
