<?php

declare(strict_types=1);

namespace Tests\Objects\Competition;

use Football\Domain\Entity\Team;
use Football\Domain\Objects\Competition\Standing;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class StandingTest extends TestCase
{
    /** @var Standing */
    private $standing;

    /** @var Team|MockObject */
    private $team;

    public function setUp(): void
    {
        $this->team = $this->createMock(Team::class);
        $this->standing = new Standing($this->team);
    }

    public function testConstruct(): void
    {
        $this->assertSame(0, $this->standing->ranking());
        $this->assertSame(0, $this->standing->matchesPlayed());
        $this->assertSame(0, $this->standing->points());
        $this->assertSame(0, $this->standing->goalsScored());
        $this->assertSame(0, $this->standing->goalsConceded());
        $this->assertSame(0, $this->standing->goalDifference());
    }

    public function testDecrease(): void
    {
        $this->standing->decreasePoints(5);
        $this->assertSame(-5, $this->standing->points());
    }

    public function testJsonSerialize(): void
    {
        $this->team->method("__toString")->willReturn("HJK");
        $expected = [
            "team" => "HJK",
            "ranking" => 0,
            "matchesPlayed" => 0,
            "points" => 0,
            "goalDifference" => 0,
            "goalsScored" => 0,
            "goalsConceded" => 0
        ];
        $this->assertSame($expected, $this->standing->jsonSerialize());
    }

    public function testRank(): void
    {
        $this->standing->rank(5);
        $this->assertSame(5, $this->standing->ranking());
    }

    public function testAddResultWhenLost(): void
    {
        $this->standing->addResult(1, 4);
        $this->assertSame(0, $this->standing->ranking());
        $this->assertSame(1, $this->standing->matchesPlayed());
        $this->assertSame(0, $this->standing->points());
        $this->assertSame(1, $this->standing->goalsScored());
        $this->assertSame(4, $this->standing->goalsConceded());
        $this->assertSame(-3, $this->standing->goalDifference());
    }

    public function testAddResultWhenDraw(): void
    {
        $this->standing->addResult(1, 1);
        $this->assertSame(0, $this->standing->ranking());
        $this->assertSame(1, $this->standing->matchesPlayed());
        $this->assertSame(1, $this->standing->points());
        $this->assertSame(1, $this->standing->goalsScored());
        $this->assertSame(1, $this->standing->goalsConceded());
        $this->assertSame(0, $this->standing->goalDifference());
    }

    public function testAddResultWhenWon(): void
    {
        $this->standing->addResult(4, 1);
        $this->assertSame(0, $this->standing->ranking());
        $this->assertSame(1, $this->standing->matchesPlayed());
        $this->assertSame(3, $this->standing->points());
        $this->assertSame(4, $this->standing->goalsScored());
        $this->assertSame(1, $this->standing->goalsConceded());
        $this->assertSame(3, $this->standing->goalDifference());
    }

    public function comparisonProvider(): array
    {
        $standings = [];

        $standing1 = $this->getStandingWithResult(1, 4, null);
        $standing2 = $this->getStandingWithResult(4, 1, null);
        $standings[] = [$standing1, $standing2, 1];

        $standing3 = $this->getStandingWithResult(5, 0, null);
        $standing4 = $this->getStandingWithResult(0, 1, null);
        $standings[] = [$standing3, $standing4, -1];

        $standing5 = $this->getStandingWithResult(5, 0, null);
        $standing6 = $this->getStandingWithResult(5, 1, null);
        $standings[] = [$standing5, $standing6, -1];

        $standing7 = $this->getStandingWithResult(1, 0, null);
        $standing8 = $this->getStandingWithResult(5, 1, null);
        $standings[] = [$standing7, $standing8, 1];

        $standing9 = $this->getStandingWithResult(5, 5, null);
        $standing10 = $this->getStandingWithResult(1, 1, null);
        $standings[] = [$standing9, $standing10, -1];

        $standing11 = $this->getStandingWithResult(1, 1, null);
        $standing12 = $this->getStandingWithResult(4, 4, null);
        $standings[] = [$standing11, $standing12, 1];

        $team1 = $this->createMock(Team::class);
        $team1->method("__toString")->willReturn("A-Team");
        $standing13 = $this->getStandingWithResult(5, 5, $team1);
        $team2 = $this->createMock(Team::class);
        $team2->method("__toString")->willReturn("B-Team");
        $standing14 = $this->getStandingWithResult(5, 5, $team2);
        $standings[] = [$standing13, $standing14, -1];

        return $standings;
    }

    /**
     * @param Standing $standing1
     * @param Standing $standing2
     * @param int $expected
     * @dataProvider comparisonProvider
     */
    public function testCompareTo(Standing $standing1, Standing $standing2, int $expected): void
    {
        $this->assertSame($expected, $standing1->compareTo($standing2));
    }

    private function getStandingWithResult(int $teamGoals, int $opponentGoals, ?Team $team): Standing
    {
        if ($team === null) {
            $team = $this->createMock(Team::class);
        }
        $standing = new Standing($team);
        $standing->addResult($teamGoals, $opponentGoals);
        return $standing;
    }
}
