<?php

declare(strict_types=1);

namespace Tests\Objects\Competition;

use Doctrine\Common\Collections\ArrayCollection;
use Exception;
use Football\Domain\Entity\Club;
use Football\Domain\Entity\Competition;
use Football\Domain\Entity\SoccerMatch;
use Football\Domain\Entity\PointReduction;
use Football\Domain\Entity\Team;
use Football\Domain\Objects\Competition\Standings;
use Football\Domain\Objects\Match\MatchResult;
use Football\Domain\Objects\Match\Score;
use Football\Domain\Objects\SocialMedia;
use PHPUnit\Framework\TestCase;

class StandingsTest extends TestCase
{
    private Standings $standings;

    public function setUp(): void
    {
        $this->standings = new Standings();
    }

    public function testAddPointReductions(): void
    {
        $teams = new ArrayCollection();
        $team = $this->getTeam("HJK");
        $teams->add($team);
        $this->standings->addTeams($teams);
        $reductions = new ArrayCollection();
        $reductions->add($this->getPointReductionFor($team, 5));
        $this->standings->addPointReductions($reductions);
        $this->assertSame(-5, $this->standings->getIterator()->current()->points());
    }

    public function testAddPointReductionsThrowsException(): void
    {
        $reductions = new ArrayCollection();
        $reductions->add($this->getPointReductionFor($this->getTeam("HJK"), 5));
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Could not find Standing for team HJK");
        $this->standings->addPointReductions($reductions);
    }

    public function testJsonSerialize(): void
    {
        $expected = ["standings" => []];
        $this->assertSame($expected, $this->standings->jsonSerialize());
    }

    public function testAddMatches(): void
    {
        $team1 = $this->getTeam("HIFK");
        $team2 = $this->getTeam("HJK");
        $this->standings->addTeams(new ArrayCollection([$team1, $team2]));
        $result = new MatchResult($team2, $team1, new Score(5, 0), new Score(3, 0));
        $this->standings->addMatches(new ArrayCollection([$this->getMatch($team2, $team1, $result)]));
        $this->assertTrue($team2->equals($this->standings->getIterator()->current()->team()));
    }

    public function testAddMatchesThrowsException(): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Could not find Standing for team ");
        $team = $this->createMock(Team::class);
        $result = new MatchResult(
            $team,
            $team,
            new Score(1, 2),
            new Score(2, 3)
        );
        $this->standings->addMatches(new ArrayCollection([$this->getMatch($team, $team, $result)]));
    }

    private function getMatch(Team $homeTeam, Team $awayTeam, MatchResult $result): SoccerMatch
    {
        $match = $this->createMock(SoccerMatch::class);
        $match->method("isPlayed")->willReturn(true);
        $match->method("homeTeam")->willReturn($homeTeam);
        $match->method("awayTeam")->willReturn($awayTeam);
        $match->method("result")->willReturn($result);
        return $match;
    }

    private function getTeam(string $teamName): Team
    {
        return new Team($teamName, new SocialMedia(), null);
    }

    private function getPointReductionFor(Team $team, int $reduction): PointReduction
    {
        $competition = new Competition("Liiga");
        return new PointReduction($competition, $team, $reduction, "random");
    }
}
