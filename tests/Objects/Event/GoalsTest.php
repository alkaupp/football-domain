<?php

declare(strict_types=1);

namespace Tests\Objects\Event;

use DateTime;
use Football\Domain\Entity\Competition;
use Football\Domain\Entity\Event;
use Football\Domain\Entity\SoccerMatch;
use Football\Domain\Entity\Player;
use Football\Domain\Entity\Team;
use Football\Domain\Objects\Event\EventNotFoundException;
use Football\Domain\Objects\Event\Goals;
use Football\Domain\Objects\Event\PlayerAction;
use Football\Domain\Objects\Match\MatchTime;
use Football\Domain\Objects\Person;
use Football\Domain\Objects\SocialMedia;
use LogicException;
use PHPUnit\Framework\TestCase;

class GoalsTest extends TestCase
{
    private SoccerMatch $match;
    private Team $homeTeam;
    private Team $awayTeam;
    private Goals $goals;

    public function setUp(): void
    {
        $this->homeTeam = new Team("Team A", new SocialMedia(), 2000);
        $this->awayTeam = new Team("Team B", new SocialMedia(), 1999);
        $this->match = new SoccerMatch(
            $this->homeTeam,
            $this->awayTeam,
            new DateTime(),
            new Competition("Competition"),
            null
        );
        $this->goals = new Goals($this->match);
    }

    public function testCreate(): void
    {
        $this->assertTrue($this->goals->isEmpty());
    }

    public function testToString(): void
    {
        $homePlayer = $this->getPlayer("Frank", "Riverdance");
        $awayPlayer = $this->getPlayer("Max", "Power");
        $firstGoal = $this->getGoalEvent($this->homeTeam, $homePlayer, new MatchTime(15, 0, 1));
        $secondGoal = $this->getGoalEvent($this->awayTeam, $awayPlayer, new MatchTime(60, 12, 2));
        $thirdGoal = $this->getGoalEvent($this->homeTeam, $homePlayer, new MatchTime(87, 2, 2));
        $this->goals->addEvent($firstGoal);
        $this->goals->addEvent($secondGoal);
        $this->goals->addEvent($thirdGoal);
        $expected = "16' goal Team A: Riverdance 1-0\n";
        $expected .= "61' goal Team B: Power 1-1\n";
        $expected .= "88' goal Team A: Riverdance 2-1\n";
        $this->assertSame($expected, (string) $this->goals);
    }

    public function testGetForReturnGoals(): void
    {
        $player = $this->getPlayer("Frank", "Riverdance");
        $anotherPlayer = $this->getPlayer("Max", "Power");
        $goal1 = $this->getGoalEvent($this->homeTeam, $player, new MatchTime(26, 0, 1));
        $goal2 = $this->getGoalEvent($this->homeTeam, $anotherPlayer, new MatchTime(44, 0, 1));
        $this->goals->addEvent($goal1);
        $this->goals->addEvent($goal2);
        $filteredGoals = $this->goals->for($player);
        $goal = $filteredGoals->first();
        $this->assertTrue(
            count($filteredGoals) === 1
            && $goal->player()->equals($player)
        );
    }

    public function testGetForThrowsException(): void
    {
        $this->expectException(EventNotFoundException::class);
        $this->expectExceptionMessage("Frank Riverdance has not scored in match");
        $this->goals->for($this->getPlayer("Frank", "Riverdance"));
    }

    public function testGetFirstReturnsGoal(): void
    {
        $player = $this->getPlayer("Frank", "Riverdance");
        $anotherPlayer = $this->getPlayer("Max", "Power");
        $goal1 = $this->getGoalEvent($this->homeTeam, $player, new MatchTime(26, 0, 1));
        $goal2 = $this->getGoalEvent($this->homeTeam, $anotherPlayer, new MatchTime(44, 0, 1));
        $this->goals->addEvent($goal1);
        $this->goals->addEvent($goal2);
        $goal = $this->goals->first();
        $this->assertTrue(
            $goal->player()->equals($player)
        );
    }

    public function testGetFirstThrowsException(): void
    {
        $this->expectException(LogicException::class);
        $this->expectExceptionMessage("Collection is empty!");
        $this->goals->first();
    }

    public function testHasScoredReturnsFalse(): void
    {
        $this->assertFalse($this->goals->hasScored($this->getPlayer("First", "Last")));
    }

    public function testHasScoredReturnsTrue(): void
    {
        $player = $this->getPlayer("Frank", "Riverdance");
        $goal = $this->getGoalEvent($this->homeTeam, $player, new MatchTime(26, 0, 1));
        $this->goals->addEvent($goal);
        $this->assertTrue($this->goals->hasScored($player));
    }

    public function testGoalsAreInCorrectOrder(): void
    {
        $player1 = $this->getPlayer("Frank", "Riverdance");
        $goal1 = $this->getGoalEvent($this->homeTeam, $player1, new MatchTime(12, 0, 1));
        $player2 = $this->getPlayer("Max", "Power");
        $goal2 = $this->getGoalEvent($this->awayTeam, $player2, new MatchTime(22, 0, 1));
        $player3 = $this->getPlayer("Trent", "Steel");
        $goal3 = $this->getGoalEvent($this->homeTeam, $player3, new MatchTime(55, 0, 2));
        $expected = "13' goal Team A: Riverdance 1-0\n";
        $expected .= "23' goal Team B: Power 1-1\n";
        $expected .= "56' goal Team A: Steel 2-1\n";
        $this->goals->addEvent($goal1);
        $this->goals->addEvent($goal2);
        $this->goals->addEvent($goal3);
        $this->assertSame($expected, (string) $this->goals);
    }

    private function getGoalEvent(Team $team, Player $player, MatchTime $time): Event
    {
        $event = new PlayerAction(
            $this->match,
            $player,
            $team,
            $time,
            Event::TYPE_GOAL
        );
        return $event;
    }

    private function getPlayer(string $firstName, string $lastName): Player
    {
        $person = $this->createMock(Person::class);
        $person->method("getFirstName")->willReturn($firstName);
        $person->method("getLastName")->willReturn($lastName);
        $player = new Player($person, null);
        return $player;
    }
}
