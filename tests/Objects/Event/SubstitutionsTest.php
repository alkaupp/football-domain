<?php

declare(strict_types=1);

namespace Tests\Objects\Event;

use Football\Domain\Entity\Event;
use Football\Domain\Entity\SoccerMatch;
use Football\Domain\Entity\Player;
use Football\Domain\Entity\Team;
use Football\Domain\Objects\Event\PlayerAction;
use Football\Domain\Objects\Event\Substitution;
use Football\Domain\Objects\Event\SubstitutionNotFoundException;
use Football\Domain\Objects\Event\Substitutions;
use Football\Domain\Objects\Match\MatchTime;
use Football\Domain\Objects\Person;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class SubstitutionsTest extends TestCase
{
    /** @var Substitutions */
    private $substitutions;

    /** @var Substitution|MockObject */
    private $sub;

    public function setUp(): void
    {
        $this->substitutions = new Substitutions($this->createMock(SoccerMatch::class));
        $this->sub = $this->createMock(Substitution::class);
    }

    public function testAddSubstitution(): void
    {
        $this->assertTrue($this->substitutions->isEmpty());
        $this->substitutions->addSubstitution($this->sub);
        $this->assertCount(1, $this->substitutions);
    }

    public function testIsSubstitutedReturnsTrue(): void
    {
        $inPlayer = $this->getPlayer("Max", "Power");
        $outPlayer = $this->getPlayer("Frank", "Riverdance");
        $this->sub->method("inPlayer")->willReturn($inPlayer);
        $this->sub->method("outPlayer")->willReturn($outPlayer);
        $this->substitutions->addSubstitution($this->sub);
        $this->assertTrue(
            $this->substitutions->isSubstituted($inPlayer)
            && $this->substitutions->isSubstituted($outPlayer)
        );
    }

    public function testIsSubstitutedReturnsFalse(): void
    {
        $this->sub->method("inPlayer")->willReturn($this->getPlayer("Frank", "Riverdance"));
        $this->sub->method("outPlayer")->willReturn($this->getPlayer("Max", "Power"));
        $this->substitutions->addSubstitution($this->sub);
        $this->assertFalse($this->substitutions->isSubstituted($this->getPlayer("Doctor", "Evil")));
    }

    public function testGetForReturnsPlayer(): void
    {
        $inPlayer = $this->getPlayer("Frank", "Riverdance");
        $outPlayer = $this->getPlayer("Max", "Power");
        $this->sub->method("inPlayer")->willReturn($inPlayer);
        $this->sub->method("outPlayer")->willReturn($outPlayer);
        $this->substitutions->addSubstitution($this->sub);
        $substitution = $this->substitutions->for($inPlayer);
        $this->assertInstanceOf(Substitution::class, $substitution);
        $this->assertTrue($inPlayer->equals($substitution->inPlayer()));
    }

    public function testGetForThrowsException(): void
    {
        $this->expectException(SubstitutionNotFoundException::class);
        $this->expectExceptionMessage("Substitution not found for player Frank Riverdance");
        $this->substitutions->for($this->getPlayer("Frank", "Riverdance"));
    }

    public function testCreateWithDoubleSubstitution(): void
    {
        $homeTeam = $this->createMock(Team::class);
        $awayTeam = $this->createMock(Team::class);
        $homeSubs = $this->getHomeSubstitutions($homeTeam);
        $awaySubs = $this->getAwaySubstitutions($awayTeam);

        $substitutions = new Substitutions($this->createMock(SoccerMatch::class));
        foreach (array_merge($homeSubs, $awaySubs) as $sub) {
            $substitutions->addEvent($sub);
        }
        $this->assertCount(6, $substitutions);
    }

    private function getHomeSubstitutions(Team $team): array
    {
        $eventData = [
            ["Frank", "Riverdance", new MatchTime(60, 0, 2), Event::TYPE_SUBSTITUTION_IN],
            ["Max", "Power", new MatchTime(60, 0, 2), Event::TYPE_SUBSTITUTION_OUT],
            ["Trent", "Steel", new MatchTime(71, 0, 2), Event::TYPE_SUBSTITUTION_IN],
            ["Fack", "Off", new MatchTime(71, 0, 2), Event::TYPE_SUBSTITUTION_OUT],
            ["Mighty", "Ducks", new MatchTime(71, 0, 2), Event::TYPE_SUBSTITUTION_IN],
            ["Quacky", "McQuackFace", new MatchTime(71, 0, 2), Event::TYPE_SUBSTITUTION_OUT]
        ];
        return $this->createSubstitutionEvents($eventData, $team);
    }

    private function getAwaySubstitutions(Team $team): array
    {
        $eventData = [
            ["James", "Bond", new MatchTime(56, 0, 2), Event::TYPE_SUBSTITUTION_IN],
            ["Luke", "Skywalker", new MatchTime(56, 0, 2), Event::TYPE_SUBSTITUTION_OUT],
            ["Peter", "Griffin", new MatchTime(74, 0, 2), Event::TYPE_SUBSTITUTION_IN],
            ["Uuno", "Turhapuro", new MatchTime(74, 0, 2), Event::TYPE_SUBSTITUTION_OUT],
            ["Spede", "Pasanen", new MatchTime(78, 0, 2), Event::TYPE_SUBSTITUTION_IN],
            ["Funky", "Fella", new MatchTime(78, 0, 2), Event::TYPE_SUBSTITUTION_OUT]
        ];
        return $this->createSubstitutionEvents($eventData, $team);
    }

    private function getPlayer(string $firstName, string $lastName): Player
    {
        $person = $this->createMock(Person::class);
        $person->method("getLastName")->willReturn($lastName);
        $person->method("getFirstName")->willReturn($firstName);
        return new Player($person, null);
    }

    private function createSubstitutionEvents(array $eventData, Team $team): array
    {
        $substitutions = array_map(
            function (array $playerData) use ($team) {
                list($firstName, $lastName, $matchTime, $eventType) = $playerData;
                return $this->createSubstitutionEvent(
                    $team,
                    $this->getPlayer($firstName, $lastName),
                    $eventType,
                    $matchTime
                );
            },
            $eventData
        );
        return $substitutions;
    }

    private function createSubstitutionEvent(Team $team, Player $player, string $type, MatchTime $time): Event
    {
        $match = $this->createMock(SoccerMatch::class);
        $event = new PlayerAction($match, $player, $team, $time, $type);
        return $event;
    }
}
