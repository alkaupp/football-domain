<?php

declare(strict_types=1);

namespace Tests\Objects\Event;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Football\Domain\Entity\Event;
use Football\Domain\Entity\SoccerMatch;
use Football\Domain\Entity\Player;
use Football\Domain\Entity\Team;
use Football\Domain\Objects\Event\Bookings;
use Football\Domain\Objects\Event\EventNotFoundException;
use Football\Domain\Objects\Event\PlayerAction;
use Football\Domain\Objects\Match\MatchTime;
use Football\Domain\Objects\Person;
use PHPUnit\Framework\TestCase;

class BookingsTest extends TestCase
{
    public function bookingTypeProvider(): array
    {
        return [
            [Event::TYPE_YELLOW_CARD],
            [Event::TYPE_SECOND_YELLOW_CARD],
            [Event::TYPE_RED_CARD]
        ];
    }

    public function sendingOffTypesProvider(): array
    {
        return [
            [Event::TYPE_SECOND_YELLOW_CARD],
            [Event::TYPE_RED_CARD]
        ];
    }

    public function testAddEvents(): void
    {
        $bookings = new Bookings();
        foreach ($this->getBookingEvents() as $event) {
            $bookings->addEvent($event);
        }
        $this->assertCount(3, $bookings);
    }

    /**
     * @param string $type
     * @dataProvider sendingOffTypesProvider
     */
    public function testIsSentOffReturnsTrue(string $type): void
    {
        $player = $this->getPlayer();
        $event = $this->getEvent($player, $type);
        $bookings = new Bookings();
        $bookings->addEvent($event);
        $this->assertTrue($bookings->isSentOff($player));
    }

    public function testIsSentOffReturnsFalse(): void
    {
        $bookings = new Bookings();
        $player = $this->getPlayer();
        $this->assertFalse($bookings->isSentOff($player));
    }

    /**
     * @param string $type
     * @dataProvider bookingTypeProvider
     */
    public function testIsBookedReturnsTrue(string $type): void
    {
        $player = $this->getPlayer();
        $event = $this->getEvent($player, $type);
        $bookings = new Bookings();
        $bookings->addEvent($event);
        $this->assertTrue($bookings->isBooked($player));
    }

    public function testIsBookedReturnsFalse(): void
    {
        $bookings = new Bookings();
        $player = $this->getPlayer();
        $this->assertFalse($bookings->isBooked($player));
    }

    public function testGetBookingForReturnsEvent(): void
    {
        $player = $this->getPlayer();
        $yellowCard = $this->getEvent($player, Event::TYPE_YELLOW_CARD);
        $redCard = $this->getEvent($player, Event::TYPE_RED_CARD);
        $bookings = new Bookings();
        $bookings->addEvent($yellowCard);
        $bookings->addEvent($redCard);
        $booking = $bookings->bookingFor($player);
        $this->assertTrue(
            $booking instanceof Event
            && $booking->name() === Event::TYPE_YELLOW_CARD
            && $booking->player()->equals($player)
        );
    }

    public function testGetBookingForThrowsException(): void
    {
        $bookings = new Bookings();
        $this->expectException(EventNotFoundException::class);
        $this->expectExceptionMessage("No booking found for Max Power");
        $bookings->bookingFor($this->getPlayer());
    }

    /**
     * @param string $sendingOffType
     * @dataProvider sendingOffTypesProvider
     * @throws EventNotFoundException
     */
    public function testGetSendingOffForReturnsEvent(string $sendingOffType): void
    {
        $player = $this->getPlayer();
        $yellowCard = $this->getEvent($player, Event::TYPE_YELLOW_CARD);
        $redCard = $this->getEvent($player, $sendingOffType);
        $bookings = new Bookings();
        $bookings->addEvent($yellowCard);
        $bookings->addEvent($redCard);
        $booking = $bookings->sendingOffFor($player);
        $this->assertTrue(
            $booking instanceof Event
            && $booking->name() === $sendingOffType
            && $booking->player()->equals($player)
        );
    }

    public function testGetSendingOffForThrowsException(): void
    {
        $bookings = new Bookings();
        $this->expectException(EventNotFoundException::class);
        $this->expectExceptionMessage("Max Power hasn't been sent off");
        $bookings->sendingOffFor($this->getPlayer());
    }

    private function getBookingEvents(): Collection
    {
        $eventTypes = [
            Event::TYPE_YELLOW_CARD,
            Event::TYPE_SECOND_YELLOW_CARD,
            Event::TYPE_RED_CARD
        ];
        $collection = new ArrayCollection();
        foreach ($eventTypes as $type) {
            $event = $this->getEvent($this->getPlayer(), $type);
            $collection->add($event);
        }
        return $collection;
    }

    private function getEvent(Player $player, string $type): Event
    {
        $event = new PlayerAction(
            $this->createMock(SoccerMatch::class),
            $player,
            $this->createMock(Team::class),
            new MatchTime(12, 0, 1),
            $type
        );
        return $event;
    }

    private function getPlayer(): Player
    {
        $person = $this->createMock(Person::class);
        $person->method("getFirstName")->willReturn("Max");
        $person->method("getLastName")->willReturn("Power");
        $player = new Player($person, null);
        return $player;
    }
}
