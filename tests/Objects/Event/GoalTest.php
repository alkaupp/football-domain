<?php

declare(strict_types=1);

namespace Tests\Objects\Event;

use Football\Domain\Entity\Event;
use Football\Domain\Entity\SoccerMatch;
use Football\Domain\Entity\Player;
use Football\Domain\Entity\Team;
use Football\Domain\Objects\Event\Goal;
use Football\Domain\Objects\Event\OwnGoal;
use Football\Domain\Objects\Event\PenaltyGoal;
use Football\Domain\Objects\Event\PlayerAction;
use Football\Domain\Objects\Match\MatchTime;
use Football\Domain\Objects\Match\Score;
use Football\Domain\Objects\Person;
use Football\Domain\Objects\SocialMedia;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class GoalTest extends TestCase
{
    public function goalProvider(): array
    {
        return [
            [Event::TYPE_GOAL, Goal::class],
            [Event::TYPE_OWN_GOAL, OwnGoal::class],
            [Event::TYPE_PENALTY_GOAL, PenaltyGoal::class]
        ];
    }

    /**
     * @param string $eventType
     * @param string $expected
     * @dataProvider goalProvider
     */
    public function testCreateFromEvent(string $eventType, string $expected): void
    {
        $event = $this->getEvent($eventType);
        $team = new Team("HJK", new SocialMedia(), null);
        $goal = Goal::createFromEvent($event, new Score(1, 1), $team);
        $this->assertInstanceOf($expected, $goal);
    }

    public function testToString(): void
    {
        $player = new Player(new Person("Alfredo", "Morelos", new \DateTime(), null, null, null, null), null);
        $team = new Team("HJK", new SocialMedia(), null);
        $goal = new Goal($player, new MatchTime(24, 0, 1), new Score(1, 0), $team);
        $this->assertSame("25' goal HJK: Morelos 1-0", $goal->__toString());
    }

    public function invalidEventProvider(): array
    {
        return [
            [Event::TYPE_SUBSTITUTION_OUT],
            [Event::TYPE_SUBSTITUTION_IN],
            [Event::TYPE_YELLOW_CARD],
            [Event::TYPE_SECOND_YELLOW_CARD],
            [Event::TYPE_RED_CARD]
        ];
    }

    /**
     * @param string $eventType
     * @dataProvider invalidEventProvider
     */
    public function testCreateFromEventThrowsException(string $eventType): void
    {
        $event = $this->getEvent($eventType);
        $team = new Team("HJK", new SocialMedia(), null);
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Invalid goal type: {$eventType}");
        Goal::createFromEvent($event, new Score(0, 1), $team);
    }

    private function getEvent(string $type): Event
    {
        $match = $this->createMock(SoccerMatch::class);
        $player = new Player(new Person("Alfredo", "Morelos", new \DateTime(), null, null, null, null), null);
        $team = new Team("HJK", new SocialMedia(), null);
        $matchTime = new MatchTime(65, 0, 2);
        return new PlayerAction($match, $player, $team, $matchTime, $type);
    }
}
