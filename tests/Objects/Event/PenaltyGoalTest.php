<?php

declare(strict_types=1);

namespace Tests\Objects\Event;

use Football\Domain\Entity\Player;
use Football\Domain\Entity\Team;
use Football\Domain\Objects\Event\PenaltyGoal;
use Football\Domain\Objects\Match\MatchTime;
use Football\Domain\Objects\Match\Score;
use Football\Domain\Objects\Person;
use Football\Domain\Objects\SocialMedia;
use PHPUnit\Framework\TestCase;

class PenaltyGoalTest extends TestCase
{
    private PenaltyGoal $penGoal;

    public function setUp(): void
    {
        $team = new Team("HJK", new SocialMedia(), 1907);
        $player = new Player(new Person("Dan", "Deansy", new \DateTime(), null, null, null, null), null);
        $this->penGoal = new PenaltyGoal($player, new MatchTime(15, 55, 1), new Score(2, 2), $team);
    }

    public function testToString(): void
    {
        $expected = "16' goal (PG) HJK: Deansy 2-2";
        $this->assertSame($expected, (string) $this->penGoal);
    }

    public function testName(): void
    {
        $this->assertSame("penalty goal", $this->penGoal->name());
    }
}
