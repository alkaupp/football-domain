<?php

declare(strict_types=1);

namespace Tests\Objects\Event;

use Football\Domain\Entity\Club;
use Football\Domain\Entity\Player;
use Football\Domain\Entity\Team;
use Football\Domain\Objects\Event\Substitution;
use Football\Domain\Objects\Match\MatchTime;
use Football\Domain\Objects\Person;
use Football\Domain\Objects\SocialMedia;
use PHPUnit\Framework\TestCase;

class SubstitutionTest extends TestCase
{
    private Player $inPlayer;
    private Player $outPlayer;

    public function setUp(): void
    {
        $this->outPlayer = new Player(new Person('Alfredo', 'Morelos', new \DateTime(), null, null, null, null), null);
        $this->inPlayer = new Player(new Person('Jari', 'Litmanen', new \DateTime(), null, null, null, null), null);
    }

    public function testConstruct(): void
    {
        $time = new MatchTime(55, 0, 2);
        $substitution = new Substitution($this->inPlayer, $this->outPlayer, $time, $this->createMock(Team::class));
        $this->assertSame($substitution->time()->toFullMinutes(), 55);
    }

    public function testToString(): void
    {
        $team = new Team('HJK', new SocialMedia(), null);
        $substitution = new Substitution($this->inPlayer, $this->outPlayer, new MatchTime(35, 00, 1), $team);
        $expected = "36' substitution HJK: IN Litmanen, OUT Morelos";
        $this->assertSame($expected, (string) $substitution);
    }
}
