<?php

declare(strict_types=1);

namespace Tests\Objects\Event;

use Football\Domain\Entity\Player;
use Football\Domain\Entity\Team;
use Football\Domain\Objects\Event\OwnGoal;
use Football\Domain\Objects\Match\MatchTime;
use Football\Domain\Objects\Match\Score;
use Football\Domain\Objects\Person;
use Football\Domain\Objects\SocialMedia;
use PHPUnit\Framework\TestCase;

class OwnGoalTest extends TestCase
{
    private OwnGoal $ownGoal;

    public function setUp(): void
    {
        $team = new Team("HJK", new SocialMedia(), 1907);
        $player = new Player(new Person("Dan", "Deansy", new \DateTime(), null, null, null, null), null);
        $this->ownGoal = new OwnGoal($player, new MatchTime(15, 55, 1), new Score(2, 2), $team);
    }

    public function testToString(): void
    {
        $expected = "16' goal (OG) HJK: Deansy 2-2";
        $this->assertSame($expected, (string) $this->ownGoal);
    }

    public function testName(): void
    {
        $this->assertSame("own goal", $this->ownGoal->name());
    }
}
