<?php

declare(strict_types=1);

namespace Tests\Objects\Event;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Football\Domain\Entity\Event;
use Football\Domain\Entity\SoccerMatch;
use Football\Domain\Entity\Player;
use Football\Domain\Entity\Team;
use Football\Domain\Objects\Event\Goal;
use Football\Domain\Objects\Event\MatchEvents;
use Football\Domain\Objects\Event\PlayerAction;
use Football\Domain\Objects\Event\Substitution;
use Football\Domain\Objects\Match\MatchTime;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class MatchEventsTest extends TestCase
{
    /** @var Player|MockObject */
    private $player;

    public function setUp(): void
    {
        $this->player = $this->createMock(Player::class);
    }

    public function testCreate(): void
    {
        $matchEvents = new MatchEvents($this->createMock(SoccerMatch::class));
        foreach ($this->createEvents() as $event) {
            $matchEvents->addEvent($event);
        }
        $this->assertCount(1, $matchEvents->goals());
        $this->assertCount(1, $matchEvents->substitutions());
        $this->assertCount(3, $matchEvents->bookings());
    }

    public function testIterator(): void
    {
        $matchEvents = new MatchEvents($this->createMock(SoccerMatch::class));
        foreach ($this->createEvents() as $event) {
            $matchEvents->addEvent($event);
        }
        $iterator = $matchEvents->getIterator();
        $this->assertInstanceOf(Goal::class, $iterator->current());
        $iterator->next();
        $this->assertInstanceOf(Substitution::class, $iterator->current());
        $iterator->next();
        $this->assertSame(Event::TYPE_YELLOW_CARD, $iterator->current()->name());
        $iterator->next();
        $this->assertSame(Event::TYPE_SECOND_YELLOW_CARD, $iterator->current()->name());
        $iterator->next();
        $this->assertSame(Event::TYPE_RED_CARD, $iterator->current()->name());
    }

    private function createEvents(): Collection
    {
        $goal = $this->getEvent(Event::TYPE_GOAL, new MatchTime(15, 0, 1), $this->player);
        $substitutePlayer = $this->createMock(Player::class);
        $substitutePlayer->expects($this->once())->method('equals')->willReturn(true);
        $substitutionIn = $this->getEvent(Event::TYPE_SUBSTITUTION_IN, new MatchTime(60, 0, 2), $substitutePlayer);
        $startingPlayer = $this->createMock(Player::class);
        $startingPlayer->method('equals')->willReturn(false);
        $substitutionOut = $this->getEvent(Event::TYPE_SUBSTITUTION_OUT, new MatchTime(60, 0, 2), $startingPlayer);
        $booking = $this->getEvent(Event::TYPE_YELLOW_CARD, new MatchTime(65, 0, 2), $this->player);
        $secondYellow = $this->getEvent(Event::TYPE_SECOND_YELLOW_CARD, new MatchTime(68, 0, 2), $this->player);
        $redCard = $this->getEvent(Event::TYPE_RED_CARD, new MatchTime(80, 0, 2), $this->player);
        $events = [
            $goal,
            $substitutionIn,
            $substitutionOut,
            $booking,
            $secondYellow,
            $redCard
        ];
        return new ArrayCollection($events);
    }

    private function getEvent(string $name, MatchTime $time, Player $player): Event
    {
        $match = $this->createMock(SoccerMatch::class);
        return new PlayerAction($match, $player, $this->createMock(Team::class), $time, $name);
    }
}
