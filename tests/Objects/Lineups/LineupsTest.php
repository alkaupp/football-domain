<?php

declare(strict_types=1);

namespace Tests\Objects\Lineups;

use DateTime;
use DomainException;
use Football\Domain\Entity\Competition;
use Football\Domain\Entity\Lineup;
use Football\Domain\Entity\Lineup\StartingLineup;
use Football\Domain\Entity\SoccerMatch;
use Football\Domain\Entity\Player;
use Football\Domain\Entity\Team;
use Football\Domain\Objects\Lineups\Lineups;
use Football\Domain\Objects\Lineups\LineupsException;
use Football\Domain\Objects\Person;
use Football\Domain\Objects\Position;
use Football\Domain\Objects\SocialMedia;
use PHPUnit\Framework\TestCase;

class LineupsTest extends TestCase
{
    private Lineups $lineups;
    private SoccerMatch $match;
    private Team $homeTeam;
    private Team $awayTeam;

    public function setUp(): void
    {
        $this->homeTeam = new Team("Team A", new SocialMedia(), 1907);
        $this->awayTeam = new Team("Team B", new SocialMedia(), 1907);
        $this->match = new SoccerMatch(
            $this->homeTeam,
            $this->awayTeam,
            new DateTime(),
            new Competition("Competition"),
            null
        );
        $this->lineups = new Lineups($this->match);
    }

    public function testAddLineupThrowsDomainException(): void
    {
        $this->expectException(DomainException::class);
        $this->expectExceptionMessage("Lineup is not part of match.");
        $this->lineups->addLineup($this->createMock(Lineup::class));
    }

    public function lineupProvider(): array
    {
        $lineup1 = $this->createMock(StartingLineup::class);
        $lineup1->method("isForHome")->willReturn(true);
        $lineup1->method("isPartOf")->willReturn(true);

        $lineup2 = $this->createMock(StartingLineup::class);
        $lineup2->method("isForHome")->willReturn(true);
        $lineup2->method("isPartOf")->willReturn(true);

        $lineup3 = $this->createMock(StartingLineup::class);
        $lineup3->method("isForAway")->willReturn(true);
        $lineup3->method("isPartOf")->willReturn(true);

        $lineup4 = $this->createMock(StartingLineup::class);
        $lineup4->method("isForAway")->willReturn(true);
        $lineup4->method("isPartOf")->willReturn(true);
        return [
            [$lineup1],
            [$lineup2],
            [$lineup3],
            [$lineup4],
        ];
    }

    /**
     * @param Lineup $lineup
     * @dataProvider lineupProvider
     */
    public function testAddLineup(Lineup $lineup): void
    {
        $this->lineups->addLineup($lineup);
        $this->assertCount(1, $this->lineups);
    }

    public function testJsonSerialize(): void
    {
        $expected = [
            "home" => [
                "starting" => [],
                "substitutes" => []
            ],
            "away" => [
                "starting" => [],
                "substitutes" => []
            ]
        ];
        $this->assertSame(json_encode($expected), json_encode($this->lineups));
    }

    public function testGetFor(): void
    {
        $person = new Person("Frank", "Riverdance", new DateTime(), null, null, null, null);
        $player = new Player($person, null);
        $lineup = new StartingLineup(
            $this->match,
            $player,
            $this->homeTeam,
            15,
            new Position("ST")
        );
        $this->lineups->addLineup($lineup);
        $this->assertTrue(
            $this->lineups->for($player)
                ->player()
                ->equals($player)
        );
    }

    public function testGetForThrowsLineupsException(): void
    {
        $person = new Person("Frank", "Riverdance", new DateTime(), null, null, null, null);
        $player = new Player($person, null);
        $this->expectException(LineupsException::class);
        $this->expectExceptionMessage("No lineup for player Frank Riverdance");
        $this->lineups->for($player);
    }
}
