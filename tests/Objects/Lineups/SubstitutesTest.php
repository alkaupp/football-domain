<?php

declare(strict_types=1);

namespace Tests\Objects\Lineups;

use Football\Domain\Entity\Lineup\Substitute;
use Football\Domain\Objects\Lineups\LineupsException;
use Football\Domain\Objects\Lineups\Substitutes;
use PHPUnit\Framework\TestCase;

class SubstitutesTest extends TestCase
{
    public function testLineupsThrowsLineupsExceptionWhenLineupsAreFull(): void
    {
        $lineup = $this->createMock(Substitute::class);
        $startingLineups = new Substitutes();
        while (count($startingLineups) < 7) {
            $startingLineups->add($lineup);
        }
        $this->expectException(LineupsException::class);
        $this->expectExceptionMessage("Substitutes already contains 7 players");
        $startingLineups->add($lineup);
    }
}
