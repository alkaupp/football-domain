<?php

declare(strict_types=1);

namespace Tests\Objects\Lineups;

use DateTime;
use Football\Domain\Entity\Lineup\StartingLineup;
use Football\Domain\Entity\Player;
use Football\Domain\Objects\Lineups\StartingLineups;
use Football\Domain\Objects\Lineups\LineupsException;
use Football\Domain\Objects\Person;
use PHPUnit\Framework\TestCase;

class StartingLineupsTest extends TestCase
{
    public function testLineupsThrowsLineupsExceptionWhenLineupsAreFull(): void
    {
        $lineup = $this->createMock(StartingLineup::class);
        $startingLineups = new StartingLineups();
        while (count($startingLineups) < 11) {
            $startingLineups->add($lineup);
        }
        $this->expectException(LineupsException::class);
        $this->expectExceptionMessage("StartingLineups already contains 11 players");
        $startingLineups->add($lineup);
    }

    public function testAddLineupsThrowsExceptionWhenPlayerAlreadyInLineups(): void
    {
        $person = $this->createMock(Person::class);
        $person->method("getFirstName")->willReturn("Frank");
        $person->method("getLastName")->willReturn("Riverdance");
        $player = new Player($person, null);
        $lineup = $this->createMock(StartingLineup::class);
        $lineup->method("player")->willReturn($player);
        $lineups = new StartingLineups();
        $lineups->add($lineup);
        $this->expectException(LineupsException::class);
        $this->expectExceptionMessage("Player Frank Riverdance is already in lineups!");
        $lineups->add($lineup);
    }

    public function testForReturnsLineup(): void
    {
        $player = new Player($this->createMock(Person::class), null);
        $lineup = $this->createMock(StartingLineup::class);
        $lineup->method("player")->willReturn($player);
        $lineups = new StartingLineups();
        $lineups->add($lineup);
        $this->assertTrue($player->equals($lineups->for($player)->player()));
    }

    public function testForThrowsLineupException(): void
    {
        $lineups = new StartingLineups();
        $person = new Person("Frank", "Riverdance", new DateTime(), null, null, null, null);
        $player = new Player($person, null);
        $this->expectException(LineupsException::class);
        $this->expectExceptionMessage("No lineup for player Frank Riverdance");
        $lineups->for($player);
    }
}
