<?php

declare(strict_types=1);

namespace Tests\Objects;

use Football\Domain\Objects\NullPosition;
use PHPUnit\Framework\TestCase;

class NullPositionTest extends TestCase
{
    /** @var NullPosition */
    private $position;

    public function setUp(): void
    {
        $this->position = new NullPosition();
    }

    public function testToShortDescription(): void
    {
        $this->assertSame("", $this->position->toShortDescription());
    }

    public function testToString(): void
    {
        $this->assertSame("", (string) $this->position);
    }

    public function testIsNull(): void
    {
        $this->assertTrue($this->position->isNull());
    }
}
