<?php

declare(strict_types=1);

namespace Tests\Objects\Match;

use Football\Domain\Objects\Match\MatchStatistic;
use PHPUnit\Framework\TestCase;

class MatchStatisticTest extends TestCase
{
    public function testConstruct(): void
    {
        $stat = new MatchStatistic("goals");
        $this->assertTrue(
            $stat->description() === "goals"
            && $stat->forHome() === 0
            && $stat->forAway() === 0
        );
    }

    public function testAddEvents(): void
    {
        $stat = new MatchStatistic("bookings");
        $stat->addHomeEvent();
        $stat->addHomeEvent();
        $stat->addAwayEvent();
        $this->assertTrue(
            $stat->forHome() === 2
            && $stat->forAway() === 1
        );
    }
}
