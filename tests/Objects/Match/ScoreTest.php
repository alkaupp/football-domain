<?php

declare(strict_types=1);

namespace Tests\Objects\Match;

use Football\Domain\Objects\Match\Score;
use PHPUnit\Framework\TestCase;

class ScoreTest extends TestCase
{
    public function testToString(): void
    {
        $score = new Score(3, 2);
        $this->assertSame("3 - 2", (string) $score);
    }

    public function testGetters(): void
    {
        $score = new Score(4, 0);
        $this->assertSame(4, $score->forHome());
        $this->assertSame(0, $score->forAway());
    }
}
