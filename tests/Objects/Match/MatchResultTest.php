<?php

declare(strict_types=1);

namespace Tests\Objects\Match;

use Football\Domain\Entity\Team;
use Football\Domain\Objects\Match\MatchResult;
use Football\Domain\Objects\Match\Score;
use Football\Domain\Objects\SocialMedia;
use PHPUnit\Framework\TestCase;

class MatchResultTest extends TestCase
{
    public function testToString(): void
    {
        $homeTeam = new Team('HJK', new SocialMedia(), 1907);
        $awayTeam = new Team('HIFK', new SocialMedia(), 1897);
        $ftScore = new Score(4, 2);
        $htScore = new Score(0, 2);
        $result = new MatchResult(
            $homeTeam,
            $awayTeam,
            $ftScore,
            $htScore
        );
        $expected = "HJK 4 - 2 HIFK";
        $this->assertSame($expected, (string) $result);
    }
}
