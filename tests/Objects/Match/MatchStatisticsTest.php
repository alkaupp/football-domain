<?php

declare(strict_types=1);

namespace Tests\Objects\Match;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Football\Domain\Entity\SoccerMatch;
use Football\Domain\Entity\Player;
use Football\Domain\Entity\Team;
use Football\Domain\Objects\Event\PlayerAction;
use Football\Domain\Objects\Match\MatchStatistic;
use Football\Domain\Objects\Match\MatchStatisticNotFoundException;
use Football\Domain\Objects\Match\MatchStatistics;
use Football\Domain\Objects\Match\MatchTime;
use Football\Domain\Objects\SocialMedia;
use PHPUnit\Framework\TestCase;

class MatchStatisticsTest extends TestCase
{
    private MatchStatistics $stats;

    public function setUp(): void
    {
        $this->stats = new MatchStatistics();
        foreach ($this->getEvents() as $event) {
            $this->stats->addEvent($event);
        }
    }

    public function testCreate(): void
    {
        $this->assertInstanceOf(MatchStatistics::class, $this->stats);
    }

    public function testHasStatisticReturnsFalse(): void
    {
        $this->assertFalse($this->stats->hasStatistic("gloobies processed"));
    }

    public function testHasStatisticReturnsTrue(): void
    {
        $this->assertTrue($this->stats->hasStatistic("goal"));
    }

    public function testGetStatisticThrowsException(): void
    {
        $this->expectException(MatchStatisticNotFoundException::class);
        $this->expectExceptionMessage("No statistic described blooper.");
        $this->stats->getStatistic("blooper");
    }

    public function testGetStatisticReturnsMatchStatistic(): void
    {
        $stat = $this->stats->getStatistic("goal");
        $this->assertTrue(
            $stat instanceof MatchStatistic
            && $stat->description() === "goal"
            && $stat->forAway() === 1
            && $stat->forHome() === 1
        );
    }

    public function testAddStatistic(): void
    {
        $this->stats->addStatistic("gooby");
        $stat = $this->stats->getStatistic("gooby");
        $this->assertTrue(
            $stat instanceof MatchStatistic
            && $stat->description() === "gooby"
            && $stat->forAway() === 0
            && $stat->forHome() === 0
        );
    }

    public function testIterator(): void
    {
        $stats = [];
        foreach ($this->stats as $stat) {
            $stats[] = $stat;
        }
        $this->assertCount(1, $stats);
    }

    private function getEvents(): Collection
    {
        $match = $this->createMock(SoccerMatch::class);
        $homeTeam = new Team("Team A", new SocialMedia(), 2000);
        $awayTeam = new Team("Team B", new SocialMedia(), 2000);
        $match->method("homeTeam")->willReturn($homeTeam);
        $match->method("awayTeam")->willReturn($awayTeam);
        $player = $this->createMock(Player::class);
        $event1 = new PlayerAction($match, $player, $homeTeam, new MatchTime(12, 0, 1), "goal");
        $event2 = new PlayerAction($match, $player, $awayTeam, new MatchTime(25, 0, 1), "goal");
        $events = new ArrayCollection();
        $events->add($event1);
        $events->add($event2);

        return $events;
    }
}
