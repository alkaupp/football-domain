<?php

declare(strict_types=1);

namespace Tests\Objects\Match;

use Football\Domain\Entity\Referee;
use Football\Domain\Objects\Match\RefereeTeam;
use PHPUnit\Framework\TestCase;

class RefereeTeamTest extends TestCase
{
    public function testObject(): void
    {
        $referee = new Referee("Some Referee");
        $firstAssistant = new Referee("First Assistant");
        $secondAssistant = new Referee("Second Assistant");
        $fourthOfficial = new Referee("Fourth Official");
        $refereeTeam = new RefereeTeam(
            $referee,
            $firstAssistant,
            $secondAssistant,
            $fourthOfficial
        );
        $this->assertTrue(
            $refereeTeam->referee()->equals($referee)
            && $refereeTeam->firstAssistant()->equals($firstAssistant)
            && $refereeTeam->secondAssistant()->equals($secondAssistant)
            && $refereeTeam->fourthOfficial()->equals($fourthOfficial)
        );
    }
}
