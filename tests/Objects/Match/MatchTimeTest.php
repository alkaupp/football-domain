<?php

declare(strict_types=1);

namespace Tests\Objects\Match;

use Football\Domain\Objects\Match\MatchTime;
use PHPUnit\Framework\TestCase;

class MatchTimeTest extends TestCase
{
    public function toStringProvider(): array
    {
        return [
            [10, 0, 1, "11'"],
            [0, 0, 1, "1'"],
            [44, 55, 1, "45'"],
            [45, 55, 1, "45+1'"],
            [48, 55, 1, "45+4'"],
            [45, 55, 2, "46'"],
            [89, 55, 2, "90'"],
            [90, 55, 2, "90+1'"],
            [94, 55, 2, "90+5'"]
        ];
    }

    public function toMinutesAndSecondsProvider(): array
    {
        return [
            [10, 0, 1, "10:00"],
            [0, 0, 1, "00:00"],
            [44, 55, 1, "44:55"],
            [45, 55, 1, "45+00:55"],
            [48, 55, 1, "45+03:55"],
            [45, 55, 2, "45:55"],
            [89, 55, 2, "89:55"],
            [90, 55, 2, "90+00:55"],
            [94, 55, 2, "90+04:55"]
        ];
    }

    public function comparisonProvider(): \Generator
    {
        yield [-1, new MatchTime(15, 25, 1), new MatchTime(15, 26, 1)];
        yield [1, new MatchTime(55, 25, 2), new MatchTime(15, 26, 1)];
        yield [0, new MatchTime(15, 25, 1), new MatchTime(15, 25, 1)];
    }

    /**
     * @param int $minutes
     * @param int $seconds
     * @param int $period
     * @param string $expected
     * @dataProvider toStringProvider
     */
    public function testToString(int $minutes, int $seconds, int $period, string $expected): void
    {
        $time = new MatchTime($minutes, $seconds, $period);
        $this->assertSame($expected, (string) $time);
    }

    /**
     * @param int $minutes
     * @param int $seconds
     * @param int $period
     * @param string $expected
     * @dataProvider toMinutesAndSecondsProvider
     */
    public function testToMinutesAndSeconds(int $minutes, int $seconds, int $period, string $expected): void
    {
        $time = new MatchTime($minutes, $seconds, $period);
        $this->assertSame($expected, $time->toMinutesAndSeconds());
    }

    /**
     * @param int $minutes
     * @param int $seconds
     * @param int $period
     * @param string $timeString
     * @dataProvider toMinutesAndSecondsProvider
     */
    public function testFromTimeString(int $minutes, int $seconds, int $period, string $timeString): void
    {
        $time = MatchTime::fromTimeString($timeString, $period);
        $this->assertSame($minutes, $time->toFullMinutes());
        $this->assertSame($seconds, $time->getSeconds());
    }

    /**
     * @param int $result
     * @param MatchTime $time
     * @param MatchTime $time2
     * @dataProvider comparisonProvider
     */
    public function testCompareTo(int $result, MatchTime $time, MatchTime $time2): void
    {
        $this->assertSame($result, $time->compareTo($time2));
    }
}
