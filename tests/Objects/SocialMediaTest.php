<?php

declare(strict_types=1);

namespace Tests\Objects;

use Football\Domain\Objects\SocialMedia;
use PHPUnit\Framework\TestCase;

class SocialMediaTest extends TestCase
{
    public function testGet(): void
    {
        $socialMedia = new SocialMedia();
        $socialMedia->add("facebook", "https://facebook.com");
        $this->assertEquals("https://facebook.com", $socialMedia->getFacebook());
    }

    public function testGetReturnsNull(): void
    {
        $socialMedia = new SocialMedia();
        $this->assertNull($socialMedia->getFacebook());
    }
}
