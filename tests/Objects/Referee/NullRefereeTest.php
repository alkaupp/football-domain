<?php

declare(strict_types=1);

namespace Tests\Objects\Referee;

use Football\Domain\Entity\Referee;
use Football\Domain\Objects\Referee\NullReferee;
use PHPUnit\Framework\TestCase;

class NullRefereeTest extends TestCase
{
    /** @var NullReferee */
    private $referee;

    public function setUp(): void
    {
        $this->referee = new NullReferee();
    }

    public function testIsNull(): void
    {
        $this->assertTrue($this->referee->isNull());
    }

    public function testToString(): void
    {
        $this->assertSame("", $this->referee->__toString());
    }

    public function testEquals(): void
    {
        $this->assertFalse($this->referee->equals(new Referee("Some Ref")));
    }
}
