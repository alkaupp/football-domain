<?php

declare(strict_types=1);

namespace Tests\Objects;

use Football\Domain\Objects\Position;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class PositionTest extends TestCase
{
    public function positionProvider()
    {
        return [
            ["GK", "Goalkeeper"],
            ["LB", "Defender, left"],
            ["LCB", "Defender, center left"],
            ["CB", "Defender, center"],
            ["RCB", "Defender, center right"],
            ["RB", "Defender, right"],
            ["WBL", "Wing back, left"],
            ["WBR", "Wing back, right"],
            ["DML", "Midfield, defensive left"],
            ["DMC", "Midfield, defensive center"],
            ["DMR", "Midfield, defensive right"],
            ["LM", "Midfield, left"],
            ["LCM", "Midfield, center left"],
            ["CM", "Midfield, center"],
            ["RCM", "Midfield, center right"],
            ["RM", "Midfield, right"],
            ["AML", "Midfield, attacking left"],
            ["AMC", "Midfield, attacking center"],
            ["AMR", "Midfield, attacking right"],
            ["LW", "Winger, left"],
            ["RW", "Winger, right"],
            ["LF", "Forward, left"],
            ["CF", "Forward, center"],
            ["RF", "Forward, right"],
            ["ST", "Striker"],
        ];
    }

    /**
     * @param string $shortName
     * @param string $longName
     * @dataProvider positionProvider
     */
    public function testToShortDescription(string $shortName, string $longName): void
    {
        $position = new Position($shortName);
        $this->assertSame($shortName, $position->toShortDescription());
    }

    /**
     * @param string $shortName
     * @param string $longName
     * @dataProvider positionProvider
     */
    public function testToString(string $shortName, string $longName): void
    {
        $position = new Position($shortName);
        $this->assertSame($longName, (string) $position);
    }

    public function testConstructThrowsException(): void
    {
        $position = "CCM";
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Invalid position {$position}");
        new Position($position);
    }

    public function testIsNull(): void
    {
        $position = new Position("CM");
        $this->assertFalse($position->isNull());
    }
}
