<?php

declare(strict_types=1);

namespace Tests\Service;

use Doctrine\Common\Collections\Collection;
use Football\Domain\Entity\Competition;
use Football\Domain\Objects\Competition\Standings;
use Football\Domain\Repository\CompetitionRepository;
use Football\Domain\Service\CompetitionService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class CompetitionServiceTest extends TestCase
{
    /** @var CompetitionService */
    private $service;

    /** @var CompetitionRepository|MockObject */
    private $repository;

    public function setUp(): void
    {
        $this->repository = $this->createMock(CompetitionRepository::class);
        $this->service = new CompetitionService($this->repository);
    }

    public function testGetCompetitions(): void
    {
        $this->repository->expects($this->once())
            ->method("getCompetitions")
            ->willReturn($this->createMock(Collection::class));
        $this->service->getCompetitions();
    }

    public function testGetCompetition(): void
    {
        $this->repository->expects($this->once())
            ->method("getCompetition")
            ->with($this->isInstanceOf(UuidInterface::class))
            ->willReturn($this->createMock(Competition::class));
        $this->service->getCompetition(Uuid::uuid4());
    }

    public function testGetCompetitionTeams(): void
    {
        $this->repository->expects($this->once())
            ->method("getCompetitionTeams")
            ->with($this->equalTo("league"))
            ->willReturn($this->createMock(Collection::class));
        $this->service->getCompetitionTeams("league");
    }

    public function testGetCompetitionMatches(): void
    {
        $this->repository->expects($this->once())
            ->method("getCompetitionMatches")
            ->with(
                $this->isInstanceOf(UuidInterface::class),
                $this->equalTo([])
            )
            ->willReturn($this->createMock(Collection::class));
        $this->service->getCompetitionMatches(Uuid::uuid4(), []);
    }

    public function testGetCompetitionStandings(): void
    {
        $competition = $this->createMock(Competition::class);
        $competition->expects($this->once())
            ->method("standings")
            ->willReturn($this->createMock(Standings::class));
        $this->repository->expects($this->once())
            ->method("getCompetition")
            ->with($this->isInstanceOf(UuidInterface::class))
            ->willReturn($competition);
        $this->service->getCompetitionStandings(Uuid::uuid4());
    }
}
