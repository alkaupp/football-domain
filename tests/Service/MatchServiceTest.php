<?php

declare(strict_types=1);

namespace Tests\Service;

use Football\Domain\Entity\SoccerMatch;
use Football\Domain\Entity\Team;
use Football\Domain\Objects\Lineups\Lineups;
use Football\Domain\Repository\SoccerMatchRepository;
use Football\Domain\Service\MatchService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;

class MatchServiceTest extends TestCase
{
    /** @var MatchService */
    private $matchService;

    /** @var SoccerMatchRepository|MockObject */
    private $repository;

    public function setUp(): void
    {
        $this->repository = $this->createMock(SoccerMatchRepository::class);
        $this->matchService = new MatchService($this->repository);
    }

    public function testGetMatchReturnMatch(): void
    {
        $this->repository->method("getMatch")
            ->willReturn($this->createMock(SoccerMatch::class));
        $this->assertInstanceOf(
            SoccerMatch::class,
            $this->matchService->getMatch($this->createMock(UuidInterface::class))
        );
    }

    public function testGetMatchLineupsReturnsLineups(): void
    {
        $match = $this->createMock(SoccerMatch::class);
        $homeTeam = $this->createMock(Team::class);
        $homeTeam->method("equals")->willReturn(true);
        $awayTeam = $this->createMock(Team::class);
        $match->method("homeTeam")->willReturn($homeTeam);
        $match->method("awayTeam")->willReturn($awayTeam);
        $this->repository->method("getMatch")->willReturn($match);
        $this->assertInstanceOf(
            Lineups::class,
            $this->matchService->getMatchLineups(
                $this->createMock(UuidInterface::class)
            )
        );
    }
}
