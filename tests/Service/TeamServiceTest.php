<?php

declare(strict_types=1);

namespace Tests\Service;

use Doctrine\Common\Collections\Collection;
use Football\Domain\Entity\Competition;
use Football\Domain\Entity\Team;
use Football\Domain\Repository\CompetitionRepository;
use Football\Domain\Repository\TeamRepository;
use Football\Domain\Service\TeamService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class TeamServiceTest extends TestCase
{
    /** @var TeamService */
    private $service;

    /** @var TeamRepository|MockObject */
    private $teamRepository;

    /** @var CompetitionRepository|MockObject */
    private $competitionRepository;

    public function setUp(): void
    {
        $this->teamRepository = $this->createMock(TeamRepository::class);
        $this->competitionRepository = $this->createMock(CompetitionRepository::class);
        $this->service = new TeamService($this->teamRepository, $this->competitionRepository);
    }

    public function testGetTeam(): void
    {
        $this->teamRepository->expects($this->once())
            ->method("getTeam")
            ->with($this->isInstanceOf(UuidInterface::class))
            ->willReturn($this->createMock(Team::class));
        $this->service->getTeam(Uuid::uuid4());
    }

    public function testGetTeams(): void
    {
        $this->teamRepository->expects($this->once())
            ->method("getTeams")
            ->willReturn($this->createMock(Collection::class));
        $this->service->getTeams();
    }

    public function testGetByCompetitionName(): void
    {
        $competitionName = "Competition 2018";
        $competition = $this->createMock(Competition::class);
        $this->competitionRepository->expects($this->once())
            ->method("getByName")
            ->with($this->equalTo($competitionName))
            ->willReturn($competition);
        $this->teamRepository->expects($this->once())
            ->method("getByCompetition")
            ->with($this->equalTo($competition))
            ->willReturn($this->createMock(Collection::class));
        $this->service->getByCompetitionName($competitionName);
    }

    public function testGetTeamPlayers(): void
    {
        $team = $this->createMock(Team::class);
        $team->expects($this->once())
            ->method("players")
            ->willReturn($this->createMock(Collection::class));
        $this->teamRepository->expects($this->once())
            ->method("getTeam")
            ->with($this->isInstanceOf(UuidInterface::class))
            ->willReturn($team);
        $this->service->getTeamPlayers(Uuid::uuid4());
    }
}
