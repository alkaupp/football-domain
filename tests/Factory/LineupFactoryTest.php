<?php

declare(strict_types=1);

namespace Tests\Factory;

use Football\Domain\Entity\Lineup\StartingLineup;
use Football\Domain\Entity\Lineup\Substitute;
use Football\Domain\Entity\SoccerMatch;
use Football\Domain\Entity\Player;
use Football\Domain\Entity\Team;
use Football\Domain\Factory\LineupFactory;
use Football\Domain\Objects\Position;
use PHPUnit\Framework\TestCase;

class LineupFactoryTest extends TestCase
{
    /** @var LineupFactory */
    private $factory;

    public function setUp(): void
    {
        $this->factory = new LineupFactory();
    }

    public function testCreateStarter(): void
    {
        $match = $this->createMock(SoccerMatch::class);
        $player = $this->createMock(Player::class);
        $team = $this->createMock(Team::class);
        $position = $this->createMock(Position::class);
        $starter = $this->factory->createStarter($match, $player, $team, $position, 15);
        $this->assertInstanceOf(StartingLineup::class, $starter);
    }

    public function testCreateSubstitute(): void
    {
        $match = $this->createMock(SoccerMatch::class);
        $player = $this->createMock(Player::class);
        $team = $this->createMock(Team::class);
        $substitute = $this->factory->createSubstitute($match, $player, $team, 15);
        $this->assertInstanceOf(Substitute::class, $substitute);
    }
}
