<?php

declare(strict_types=1);

namespace Tests\Entity;

use Football\Domain\Entity\Player;
use Football\Domain\Entity\Team;
use Football\Domain\Objects\Person;
use Football\Domain\Objects\SocialMedia;
use Football\Domain\Objects\Team\NullTeam;
use PHPUnit\Framework\TestCase;

class PlayerTest extends TestCase
{
    public function testEqualsReturnNull(): void
    {
        $player1 = new Player($this->createMock(Person::class), null);
        $player2 = new Player($this->createMock(Person::class), null);
        $this->assertFalse($player1->equals($player2));
    }

    public function testEqualsReturnsTrue(): void
    {
        $player1 = new Player($this->createMock(Person::class), null);
        $this->assertTrue($player1->equals($player1));
    }

    public function testShirtNameReturnsLastName(): void
    {
        $person = $this->createMock(Person::class);
        $person->method("getLastName")->willReturn("Lastname");
        $player = new Player($person, null);
        $this->assertEquals("Lastname", $player->shirtName());
    }

    public function testShirtNameReturnsShirtName(): void
    {
        $person = $this->createMock(Person::class);
        $person->method("getLastName")->willReturn("Lastname");
        $person->method("getNickName")->willReturn("Shirtname");
        $player = new Player($person, null);
        $this->assertEquals("Shirtname", $player->shirtName());
    }

    public function testJsonSerialize(): void
    {
        $player = new Player($this->createMock(Person::class), null);
        $expected = ["id", "firstName", "lastName", "playerName", "birthday", "height", "weight", "nationality"];
        $this->assertSame($expected, array_keys($player->jsonSerialize()));
    }
    
    public function testCurrentTeamReturnsHJK(): void
    {
        $player = new Player($this->createMock(Person::class), null);
        $firstTeam = new Team("Rangers", new SocialMedia(), null);
        $player->transferTo($firstTeam);
        $secondTeam = new Team("HJK", new SocialMedia(), null);
        $player->transferTo($secondTeam);
        $this->assertEquals("HJK", (string) $player->currentTeam());
    }

    public function testCurrentTeamReturnsNullTeam(): void
    {
        $player = new Player($this->createMock(Person::class), null);
        $this->assertInstanceOf(NullTeam::class, $player->currentTeam());
    }
}
