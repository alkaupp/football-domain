<?php

declare(strict_types=1);

namespace Tests\Entity;

use Football\Domain\Entity\Venue;
use Football\Domain\Objects\Address;
use PHPUnit\Framework\TestCase;

class VenueTest extends TestCase
{
    public function testToString(): void
    {
        $venue = new Venue("Telia 5G Areena", $this->createMock(Address::class));
        $this->assertSame("Telia 5G Areena", (string) $venue);
    }
}
