<?php

declare(strict_types=1);

namespace Tests\Entity;

use Football\Domain\Entity\Referee;
use PHPUnit\Framework\TestCase;

class RefereeTest extends TestCase
{
    public function testToString(): void
    {
        $referee = new Referee("Randolph Referee");
        $this->assertSame("Randolph Referee", (string) $referee);
    }

    public function testIsNull(): void
    {
        $referee = new Referee("Randolph Referee");
        $this->assertFalse($referee->isNull());
    }
}
