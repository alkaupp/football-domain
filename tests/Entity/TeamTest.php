<?php

declare(strict_types=1);

namespace Tests\Entity;

use Football\Domain\Entity\Team;
use Football\Domain\Objects\SocialMedia;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;

class TeamTest extends TestCase
{
    public function testJsonSerialize(): void
    {
        $team = new Team("Football Team", new SocialMedia(), 1907);
        $expectedKeys = ["id", "name", "founded", "socialMedia"];
        $result = $team->jsonSerialize();
        $this->assertSame($expectedKeys, array_keys($result));
        $this->assertInstanceOf(UuidInterface::class, $result["id"]);
        $this->assertSame("Football Team", $result["name"]);
        $this->assertSame(1907, $result["founded"]);
    }

    public function testGetPlayers(): void
    {
        $team = new Team("", new SocialMedia(), 1890);
        $this->assertEmpty($team->players());
    }
}
