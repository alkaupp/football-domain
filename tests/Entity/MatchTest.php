<?php

declare(strict_types=1);

namespace Tests\Entity;

use DateTime;
use Football\Domain\Entity\Club;
use Football\Domain\Entity\Competition;
use Football\Domain\Entity\Event;
use Football\Domain\Entity\Lineup\StartingLineup;
use Football\Domain\Entity\SoccerMatch;
use Football\Domain\Entity\Player;
use Football\Domain\Entity\Referee;
use Football\Domain\Entity\Team;
use Football\Domain\Objects\Event\PlayerAction;
use Football\Domain\Objects\Lineups\Lineups;
use Football\Domain\Objects\Match\MatchResult;
use Football\Domain\Objects\Match\MatchStatistics;
use Football\Domain\Objects\Match\MatchTime;
use Football\Domain\Objects\Match\Score;
use Football\Domain\Objects\Match\RefereeTeam;
use Football\Domain\Objects\Person;
use Football\Domain\Objects\SocialMedia;
use Generator;
use LogicException;
use PHPStan\Testing\TestCase;

class MatchTest extends TestCase
{
    private SoccerMatch $match;
    private Team $homeTeam;
    private Team $awayTeam;

    public function setUp(): void
    {
        $this->homeTeam = new Team('HJK', new SocialMedia(), 1907);
        $this->awayTeam = new Team('HIFK', new SocialMedia(), 1897);
        $date = new DateTime();
        $this->match = new SoccerMatch($this->homeTeam, $this->awayTeam, $date, new Competition('League'), null);
    }

    public function testIsPlannedReturnsTrue(): void
    {
        $this->assertTrue($this->match->isPlanned());
    }

    public function testIsPlayedReturnsFalse(): void
    {
        $this->assertFalse($this->match->isPlayed());
    }

    public function testIsPlayedReturnsTrue(): void
    {
        $this->match->start();
        $this->match->end(1233);
        $this->assertTrue($this->match->isPlayed());
    }

    public function testIsCanceledReturnsFalse(): void
    {
        $this->assertFalse($this->match->isCanceled());
    }

    public function testIsCanceledReturnsTrue(): void
    {
        $this->match->cancel();
        $this->assertTrue($this->match->isCanceled());
    }

    public function testIsLiveReturnsFalse(): void
    {
        $this->assertFalse($this->match->isLive());
    }

    public function testIsLiveReturnsTrue(): void
    {
        $this->match->start();
        $this->assertTrue($this->match->isLive());
    }

    public function testBeginBreakThrowsLogicException(): void
    {
        $this->expectException(LogicException::class);
        $this->expectExceptionMessage("Cannot break match that isn't live.");
        $this->match->beginBreak();
    }

    public function testEndThrowsLogicException(): void
    {
        $this->expectException(LogicException::class);
        $this->expectExceptionMessage("Cannot end match that isn't live.");
        $this->match->end(2000);
    }

    public function testGetResultThrowsExceptionMatchNotPlayed(): void
    {
        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Match has not been played yet (Planned).');
        $this->match->result();
    }

    public function testGetResultReturnsMatchResult(): void
    {
        $this->match->start();
        $this->match->end(1223);
        $expected = 'HJK 0 - 0 HIFK';
        $this->assertSame($expected, (string) $this->match->result());
    }

    public function testAssignRefereeTeam(): void
    {
        $refereeTeam = new RefereeTeam(
            new Referee('Frank Referee'),
            new Referee('Frank AsstReferee'),
            new Referee('Frank AsstReferee II'),
            new Referee('Frank Fourth')
        );
        $this->match->assignRefereeTeam($refereeTeam);
        $this->assertTrue($this->match->refereeTeamAssigned());
    }

    public function testJsonSerializeReturnsCorrectKeys(): void
    {
        $expectedKeys = [
            'id', 'startTime', 'venue', 'homeTeam', 'awayTeam', 'halfTimeScore',
            'fullTimeScore', 'referee', 'firstAssistantReferee', 'secondAssistantReferee',
            'fourthOfficial', 'status', 'attendance', 'matchEvents'
        ];
        $json = json_encode($this->match, JSON_THROW_ON_ERROR, 512);
        $actualKeys = array_keys(json_decode($json, true, 512, JSON_THROW_ON_ERROR));
        $this->assertSame($expectedKeys, $actualKeys);
    }

    public function testGetLineups(): void
    {
        $this->assertInstanceOf(Lineups::class, $this->match->lineups());
    }

    public function testGetHomeTeam(): void
    {
        $this->assertInstanceOf(Team::class, $this->match->homeTeam());
    }

    public function testGetAwayTeam(): void
    {
        $this->assertInstanceOf(Team::class, $this->match->awayTeam());
    }

    public function testAddLineup(): void
    {
        $this->match->addLineup($this->getLineup(true));
        $this->assertCount(1, $this->match->lineups());
    }

    public function testAddLineupThrowsException(): void
    {
        $this->expectException(LogicException::class);
        $this->expectExceptionMessage("Lineup doesn't belong to match");
        $this->match->addLineup($this->getLineup(false));
    }

    public function testAddEventThrowsException(): void
    {
        $event = $this->createMock(Event::class);
        $event->method('isPartOf')->willReturn(false);
        $this->expectException(LogicException::class);
        $this->expectExceptionMessage("Event doesn't belong to match");
        $this->match->addEvent($event);
    }

    public function testAddEvent(): void
    {
        $event = $this->createMock(Event::class);
        $event->expects($this->once())
            ->method('isPartOf')
            ->willReturn(true);
        $this->match->addEvent($event);
    }

    public function lineupModifyingEventType(): Generator
    {
        yield [Event::TYPE_SUBSTITUTION_IN];
        yield [Event::TYPE_SECOND_YELLOW_CARD];
        yield [Event::TYPE_RED_CARD];
    }

    /**
     * @param string $eventType
     * @throws \ReflectionException
     * @throws \Exception
     * @dataProvider lineupModifyingEventType
     */
    public function testAddEventModifiesLineup(string $eventType): void
    {
        $player = new Player(new Person('Frank', 'Riverdance', new DateTime(), null, null, null, null), null);
        $time = new MatchTime(55, 0, 2);
        $lineup = $this->createMock(StartingLineup::class);
        $lineup->method('player')->willReturn($player);
        $lineup->method('isForHome')->willReturn(true);
        $lineup->method('isPartOf')->willReturn(true);
        $lineup->expects($this->once())
            ->method('substitute')
            ->with($this->equalTo($time));
        $this->match->addLineup($lineup);
        $event = new PlayerAction($this->match, $player, $this->homeTeam, $time, $eventType);
        $this->match->addEvent($event);
    }

    public function testGetStatistics(): void
    {
        $this->assertInstanceOf(MatchStatistics::class, $this->match->statistics());
    }

    public function goalProvider(): Generator
    {
        yield ['home', Event::TYPE_GOAL, new MatchTime(55, 0, 2), new Score(0, 0), new Score(1, 0)];
        yield ['home', Event::TYPE_GOAL, new MatchTime(33, 0, 1), new Score(1, 0), new Score(1, 0)];
        yield ['away', Event::TYPE_GOAL, new MatchTime(55, 0, 2), new Score(0, 0), new Score(0, 1)];
        yield ['away', Event::TYPE_GOAL, new MatchTime(33, 0, 1), new Score(0, 1), new Score(0, 1)];
        yield ['away', Event::TYPE_PENALTY_GOAL, new MatchTime(55, 0, 2), new Score(0, 0), new Score(0, 1)];
        yield ['away', Event::TYPE_OWN_GOAL, new MatchTime(55, 0, 2), new Score(0, 0), new Score(1, 0)];
        yield ['away', Event::TYPE_OWN_GOAL, new MatchTime(12, 0, 1), new Score(1, 0), new Score(1, 0)];
        yield ['home', Event::TYPE_OWN_GOAL, new MatchTime(55, 0, 2), new Score(0, 0), new Score(0, 1)];
        yield ['home', Event::TYPE_OWN_GOAL, new MatchTime(12, 0, 1), new Score(0, 1), new Score(0, 1)];
    }

    /**
     * @param string $homeAway
     * @param string $type
     * @param MatchTime $matchTime
     * @param Score $htScore
     * @param Score $ftScore
     * @dataProvider goalProvider
     * @throws \Exception
     */
    public function testAddEventChangesResult(
        string $homeAway,
        string $type,
        MatchTime $matchTime,
        Score $htScore,
        Score $ftScore
    ): void {
        $team = $homeAway === 'home' ? $this->homeTeam : $this->awayTeam;
        $player = new Player(new Person('Frank', 'Riverdance', new DateTime(), null, null, null, null), null);
        $event = new PlayerAction($this->match, $player, $team, $matchTime, $type);
        $this->match->addEvent($event);
        $this->match->start();
        $this->match->end(30000);
        $result = new MatchResult($this->homeTeam, $this->awayTeam, $ftScore, $htScore);
        $this->assertEquals($result, $this->match->result());
    }

    private function getLineup(bool $isPartOf): StartingLineup
    {
        $lineup = $this->createMock(StartingLineup::class);
        $lineup->method('isForHome')->willReturn(true);
        $lineup->method('isPartOf')->willReturn($isPartOf);
        return $lineup;
    }
}
