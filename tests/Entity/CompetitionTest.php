<?php

declare(strict_types=1);

namespace Tests\Entity;

use DateTime;
use Football\Domain\Entity\Competition;
use Football\Domain\Entity\Team;
use Football\Domain\Objects\Competition\CompetitionException;
use Football\Domain\Objects\Competition\Standings;
use Football\Domain\Objects\SocialMedia;
use PHPUnit\Framework\TestCase;

class CompetitionTest extends TestCase
{
    private Competition $competition;

    public function setUp(): void
    {
        $this->competition = new Competition("Competition 2018");
    }

    public function testCompetitionIsPlannedReturnsTrue(): void
    {
        $this->assertTrue($this->competition->isPlanned());
    }

    public function testCompetitionIsRunningReturnsFalse(): void
    {
        $this->assertFalse($this->competition->isRunning());
    }

    public function testCompetitionIsRunningReturnsTrue(): void
    {
        $this->competition->start();
        $this->assertTrue($this->competition->isRunning());
    }

    public function testCompetitionIsFinishedReturnsFalseWhenOnlyPlanned(): void
    {
        $this->assertFalse($this->competition->isFinished());
    }

    public function testCompetitionIsFinishedReturnsFalseWhenOnlyStarted(): void
    {
        $this->competition->start();
        $this->assertFalse($this->competition->isFinished());
    }

    public function testStartThrowsCompetitionExceptionWhenAlreadyStarted(): void
    {
        $this->competition->start();
        $this->expectException(CompetitionException::class);
        $this->expectExceptionMessage("Competition is already running!");
        $this->competition->start();
    }

    public function testStartThrowsExceptionWhenCompetitionIsFinished(): void
    {
        $this->competition->start();
        $this->competition->finish();
        $this->expectException(CompetitionException::class);
        $this->expectExceptionMessage("Cannot start a competition that is finished!");
        $this->competition->start();
    }

    public function testFinishThrowsExceptionWhenNotStarted(): void
    {
        $this->expectException(CompetitionException::class);
        $this->expectExceptionMessage("Cannot finish competition that is not started!");
        $this->competition->finish();
    }

    public function testFinishThrowsExceptionWhenAlreadyFinished(): void
    {
        $this->competition->start();
        $this->competition->finish();
        $this->expectException(CompetitionException::class);
        $this->expectExceptionMessage("Competition already finished");
        $this->competition->finish();
    }

    public function testFinishThrowsExceptionWhenAllMatchesAreNotPlayed(): void
    {
        $homeTeam = new Team('HJK', new SocialMedia(), 1907);
        $awayTeam = new Team('HIFK', new SocialMedia(), 1897);
        $dateTime = new DateTime();
        $this->competition->addMatch($homeTeam, $awayTeam, $dateTime, null);
        $this->competition->start();
        $this->expectException(CompetitionException::class);
        $this->expectExceptionMessage("Cannot finish competition until all matches are played!");
        $this->competition->finish();
    }

    public function testCancelThrowsCompetitionExceptionWhenAlreadyFinished(): void
    {
        $this->competition->start();
        $this->competition->finish();
        $this->expectException(CompetitionException::class);
        $this->expectExceptionMessage("Cannot cancel a finished competition!");
        $this->competition->cancel();
    }

    public function testCancelThrowsCompetitionExceptionWhenAlreadyCanceled(): void
    {
        $this->competition->cancel();
        $this->expectException(CompetitionException::class);
        $this->expectExceptionMessage("Competition already canceled!");
        $this->competition->cancel();
    }

    public function addRelationExceptionProvider(): array
    {
        $name = "Competition 2018";
        $canceled = new Competition($name);
        $canceled->cancel();
        $running = new Competition($name);
        $running->start();
        $finished = new Competition($name);
        $finished->start();
        $finished->finish();

        return [
            [$canceled, "canceled"],
            [$running, "running"],
            [$finished, "finished"],
        ];
    }

    /**
     * @dataProvider addRelationExceptionProvider
     */
    public function testAddMatchThrowsCompetitionExceptionWhenCompetitionIsRunning(
        Competition $competition,
        string $status
    ): void {
        $homeTeam = new Team('HJK', new SocialMedia(), 1907);
        $awayTeam = new Team('HIFK', new SocialMedia(), 1897);
        $dateTime = new DateTime();
        $this->expectException(CompetitionException::class);
        $this->expectExceptionMessage("Cannot add match to a {$status} competition!");
        $competition->addMatch($homeTeam, $awayTeam, $dateTime, null);
    }

    /**
     * @param Competition $competition
     * @param string $status
     * @dataProvider addRelationExceptionProvider
     * @throws \ReflectionException
     * @throws CompetitionException
     */
    public function testAddTeamThrowsCompetitionException(Competition $competition, string $status): void
    {
        $this->expectException(CompetitionException::class);
        $this->expectExceptionMessage("Cannot add team to a {$status} competition!");
        $competition->addTeam($this->createMock(Team::class));
    }

    public function testGetStandings(): void
    {
        $this->assertInstanceOf(Standings::class, $this->competition->standings());
    }

    public function testToString(): void
    {
        $this->assertSame("Competition 2018", (string) $this->competition);
    }

    public function testJsonSerialize(): void
    {
        $this->assertEquals(["id", "name"], array_keys($this->competition->jsonSerialize()));
    }
}
