<?php

declare(strict_types=1);

namespace Tests\Entity;

use DateTime;
use Football\Domain\Entity\Competition;
use Football\Domain\Entity\Event;
use Football\Domain\Entity\SoccerMatch;
use Football\Domain\Entity\Player;
use Football\Domain\Entity\Team;
use Football\Domain\Objects\Event\PlayerAction;
use Football\Domain\Objects\Match\MatchTime;
use Football\Domain\Objects\Person;
use Football\Domain\Objects\SocialMedia;
use PHPUnit\Framework\TestCase;

class EventTest extends TestCase
{
    public function comparisonProvider(): array
    {
        $events = [];

        $event1 = $this->getEvent(1, "12:00", "goal");
        $event2 = $this->getEvent(1, "12:12", "yellow card");
        $events[] = [$event1, $event2, -1];

        $event1 = $this->getEvent(1, "12:12", "goal");
        $event2 = $this->getEvent(1, "12:00", "yellow card");
        $events[] = [$event1, $event2, 1];

        $event1 = $this->getEvent(1, "12:00", "goal");
        $event2 = $this->getEvent(1, "12:00", "yellow card");
        $events[] = [$event1, $event2, 0];

        return $events;
    }

    public function eventTypeProvider(): array
    {
        return [
            [Event::TYPE_GOAL],
            [Event::TYPE_OWN_GOAL],
            [Event::TYPE_PENALTY_GOAL],
            [Event::TYPE_SUBSTITUTION_IN],
            [Event::TYPE_SUBSTITUTION_OUT],
            [Event::TYPE_YELLOW_CARD],
            [Event::TYPE_SECOND_YELLOW_CARD],
            [Event::TYPE_RED_CARD]
        ];
    }

    /**
     * @param Event $event1
     * @param Event $event2
     * @param int $expected
     * @dataProvider comparisonProvider
     */
    public function testCompareTo(Event $event1, Event $event2, int $expected): void
    {
        $this->assertSame($expected, $event1->compareTo($event2));
    }

    public function testToString(): void
    {
        $event = $this->getEvent(1, "15:55", "goal");
        $this->assertSame("16' goal HJK: Morelos", (string) $event);
    }

    /**
     * @param string $eventType
     * @dataProvider eventTypeProvider
     */
    public function testIsGoalEvent(string $eventType): void
    {
        $event = $this->getEvent(0, "12:00", $eventType);
        $expectedResult = \in_array(
            $eventType,
            [Event::TYPE_GOAL, Event::TYPE_OWN_GOAL, Event::TYPE_PENALTY_GOAL],
            true
        );
        $this->assertSame($expectedResult, $event->isGoalEvent());
    }

    /**
     * @param string $eventType
     * @dataProvider eventTypeProvider
     */
    public function testIsSubstitutionEvent(string $eventType): void
    {
        $event = $this->getEvent(0, "12:00", $eventType);
        $expectedResult = \in_array($eventType, [Event::TYPE_SUBSTITUTION_IN, Event::TYPE_SUBSTITUTION_OUT], true);
        $this->assertSame($expectedResult, $event->isSubstitutionEvent());
    }

    /**
     * @param string $eventType
     * @dataProvider eventTypeProvider
     */
    public function testIsBookingEvent(string $eventType): void
    {
        $event = $this->getEvent(0, "12:00", $eventType);
        $expectedResult = \in_array(
            $eventType,
            [Event::TYPE_YELLOW_CARD, Event::TYPE_SECOND_YELLOW_CARD, Event::TYPE_RED_CARD],
            true
        );
        $this->assertSame($expectedResult, $event->isBookingEvent());
    }

    /**
     * @param string $eventType
     * @dataProvider eventTypeProvider
     */
    public function testIsSendingOff(string $eventType): void
    {
        $event = $this->getEvent(0, "12:00", $eventType);
        $expectedResult = \in_array(
            $eventType,
            [Event::TYPE_SECOND_YELLOW_CARD, Event::TYPE_RED_CARD],
            true
        );
        $this->assertSame($expectedResult, $event->isSendingOffEvent());
    }

    public function testIsForHomeAway(): void
    {
        $player = $this->createMock(Player::class);
        $match = $this->getMatch();
        $event = new PlayerAction($match, $player, $match->homeTeam(), new MatchTime(12, 12, 1), "goal");
        $this->assertTrue($event->isForHome());
        $this->assertFalse($event->isForAway());
    }

    public function testIsPartOfReturnsTrue(): void
    {
        $match = $this->getMatch();
        $player = $this->createMock(Player::class);
        $event = new PlayerAction($match, $player, $match->homeTeam(), new MatchTime(12, 12, 1), "goal");
        $this->assertTrue($event->isPartOf($match));
    }

    public function testIsPartOfReturnsFalse(): void
    {
        $match = $this->getMatch();
        $player = $this->createMock(Player::class);
        $event = new PlayerAction($match, $player, $match->homeTeam(), new MatchTime(12, 12, 1), "goal");
        $this->assertFalse($event->isPartOf($this->getMatch()));
    }

    private function getEvent(int $period, string $time, string $name): Event
    {
        $player = new Player(
            new Person("Alfredo", "Morelos", new DateTime(), null, null, null, null),
            null
        );
        return new PlayerAction(
            $this->createMock(SoccerMatch::class),
            $player,
            new Team("HJK", new SocialMedia(), null),
            MatchTime::fromTimeString($time, $period),
            $name
        );
    }

    private function getMatch(): SoccerMatch
    {
        $homeTeam = new Team("Team A", new SocialMedia(), 2000);
        $awayTeam = new Team("Team B", new SocialMedia(), 2001);
        return new SoccerMatch($homeTeam, $awayTeam, new DateTime(), new Competition("Competition"), null);
    }
}
