<?php

declare(strict_types=1);

namespace Tests\Entity\Lineup;

use Football\Domain\Entity\Lineup\Substitute;
use Football\Domain\Entity\SoccerMatch;
use Football\Domain\Entity\Player;
use Football\Domain\Entity\Team;
use Football\Domain\Objects\Match\MatchTime;
use PHPUnit\Framework\TestCase;

class SubstituteTest extends TestCase
{
    public function testSubstitute(): void
    {
        $sub = new Substitute(
            $this->createMock(SoccerMatch::class),
            $this->createMock(Player::class),
            $this->createMock(Team::class),
            10
        );
        $sub->substitute(new MatchTime(48, 0, 2));
        $this->assertSame(42, $sub->jsonSerialize()["playingTime"]);
    }
}
