<?php

declare(strict_types=1);

namespace Tests\Entity\Lineup;

use DateTime;
use Football\Domain\Entity\Competition;
use Football\Domain\Entity\Lineup;
use Football\Domain\Entity\Lineup\StartingLineup;
use Football\Domain\Entity\SoccerMatch;
use Football\Domain\Entity\Player;
use Football\Domain\Entity\Team;
use Football\Domain\Objects\Match\MatchTime;
use Football\Domain\Objects\Person;
use Football\Domain\Objects\Position;
use Football\Domain\Objects\SocialMedia;
use PHPUnit\Framework\TestCase;

class StartingLineupTest extends TestCase
{
    public function testJsonSerialize(): void
    {
        $lineup = $this->getLineup(new Position("FW"), 10);
        $expected = [
            "player" => "Zinedine Zidane",
            "shirtName" => "Zidane",
            "shirtNumber" => 10,
            "starting" => true,
            "position" => "FW",
            "playingTime" => 90
        ];
        $this->assertSame($expected, $lineup->jsonSerialize());
    }

    public function comparisonProvider(): array
    {
        $lineups = [];

        $lineup1 = $this->getLineup(new Position("GK"), 80);
        $lineup2 = $this->getLineup(new Position("ST"), 10);
        $lineups[] = [$lineup1, $lineup2, -1];

        $lineup3 = $this->getLineup(new Position("DEF"), 33);
        $lineup4 = $this->getLineup(new Position("ST"), 10);
        $lineups[] = [$lineup3, $lineup4, 1];

        $lineup5 = $this->getLineup(new Position("ST"), 80);
        $lineup6 = $this->getLineup(new Position("GK"), 1);
        $lineups[] = [$lineup5, $lineup6, 1];

        return $lineups;
    }

    /**
     * @param Lineup $lineup1
     * @param Lineup $lineup2
     * @param int $expected
     * @dataProvider comparisonProvider
     */
    public function testCompareTo(Lineup $lineup1, Lineup $lineup2, int $expected): void
    {
        $this->assertSame($expected, $lineup1->compareTo($lineup2));
    }

    public function testIsForHomeReturnsTrue(): void
    {
        $homeTeam = new Team("Team A", new SocialMedia(), 2000);
        $awayTeam = new Team("Team B", new SocialMedia(), 1999);
        $match = new SoccerMatch($homeTeam, $awayTeam, new DateTime(), new Competition("League"), null);
        $player = new Player(
            new Person("Mike", "McDermott", new DateTime(), null, null, null, null),
            null
        );
        $lineup = new StartingLineup($match, $player, $homeTeam, 16, new Position("ST"));
        $this->assertTrue($lineup->isForHome());
        $this->assertFalse($lineup->isForAway());
    }

    public function testIsPart(): void
    {
        $getMatch = function () {
            $team = new Team('HJK', new SocialMedia(), 1907);
            return new SoccerMatch($team, $team, new DateTime(), new Competition("Competition"), null);
        };
        $player = $this->createMock(Player::class);
        $team = new Team('HIFK', new SocialMedia(), 1897);
        $aMatch = $getMatch();
        $lineup = new StartingLineup($aMatch, $player, $team, 15, new Position("ST"));
        $this->assertTrue($lineup->isPartOf($aMatch));
        $this->assertFalse($lineup->isPartOf($getMatch()));
    }

    public function testSubstitute(): void
    {
        $lineup = $this->getLineup(new Position("ST"), 10);
        $lineup->substitute(new MatchTime(60, 0, 2));
        $this->assertSame(60, $lineup->jsonSerialize()["playingTime"]);
    }

    private function getLineup(Position $position, int $shirtNumber): StartingLineup
    {
        return new StartingLineup(
            $this->createMock(SoccerMatch::class),
            $this->getPlayer(),
            $this->getTeam(),
            $shirtNumber,
            $position
        );
    }

    private function getTeam(): Team
    {
        return new Team("A-Team", new SocialMedia(), 1907);
    }

    private function getPlayer(): Player
    {
        $person = new Person('Zinedine', 'Zidane', new DateTime(), null, null, null, null);
        return new Player($person, null);
    }
}
